<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHash2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dokter_umum', function (Blueprint $table) {
            $table->string('hash', 32)->after('tahun');
        });
        Schema::table('dokter_gigi', function (Blueprint $table) {
            $table->string('hash', 32)->after('tahun');
        });
        Schema::table('perawat', function (Blueprint $table) {
            $table->string('hash', 32)->after('tahun');
        });
        Schema::table('bidan', function (Blueprint $table) {
            $table->string('hash', 32)->after('tahun');
        });
        Schema::table('kunjungan', function (Blueprint $table) {
            $table->string('hash', 32)->after('tahun');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dokter_umum', function (Blueprint $table) {
            $table->dropColumn('hash');
        });
        Schema::table('dokter_gigi', function (Blueprint $table) {
            $table->dropColumn('hash');
        });
        Schema::table('perawat', function (Blueprint $table) {
            $table->dropColumn('hash');
        });
        Schema::table('bidan', function (Blueprint $table) {
            $table->dropColumn('hash');
        });
        Schema::table('kunjungan', function (Blueprint $table) {
            $table->dropColumn('hash');
        });
    }
}
