<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenagaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokter_umum', function (Blueprint $table) {
            $table->string('puskesmas_id', 10);
            $table->integer('jumlah');
            $table->string('tahun', 10);
            $table->timestamps();

            $table->foreign('puskesmas_id')->references('id')->on('puskesmas')->onDelete('cascade');
        });

        Schema::create('dokter_gigi', function (Blueprint $table) {
            $table->string('puskesmas_id', 10);
            $table->integer('jumlah');
            $table->string('tahun', 10);
            $table->timestamps();

            $table->foreign('puskesmas_id')->references('id')->on('puskesmas')->onDelete('cascade');
        });

        Schema::create('perawat', function (Blueprint $table) {
            $table->string('puskesmas_id', 10);
            $table->integer('jumlah');
            $table->string('tahun', 10);
            $table->timestamps();

            $table->foreign('puskesmas_id')->references('id')->on('puskesmas')->onDelete('cascade');
        });

        Schema::create('bidan', function (Blueprint $table) {
            $table->string('puskesmas_id', 10);
            $table->integer('jumlah');
            $table->string('tahun', 10);
            $table->timestamps();

            $table->foreign('puskesmas_id')->references('id')->on('puskesmas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dokter_umum', function (Blueprint $table) {
            $table->dropForeign(['puskesmas_id']);
            $table->dropColumn('puskesmas_id');
        });
        Schema::table('dokter_gigi', function (Blueprint $table) {
            $table->dropForeign(['puskesmas_id']);
            $table->dropColumn('puskesmas_id');
        });
        Schema::table('perawat', function (Blueprint $table) {
            $table->dropForeign(['puskesmas_id']);
            $table->dropColumn('puskesmas_id');
        });
        Schema::table('bidan', function (Blueprint $table) {
            $table->dropForeign(['puskesmas_id']);
            $table->dropColumn('puskesmas_id');
        });
        Schema::dropIfExists('dokter_umum');
        Schema::dropIfExists('dokter_gigi');
        Schema::dropIfExists('perawat');
        Schema::dropIfExists('bidan');
    }
}
