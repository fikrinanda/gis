<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('username', 20);
            $table->string('password', 200);
            $table->string('level', 10);
            $table->timestamps();
        });

        Schema::create('wilayah', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('nama', 50);
            $table->string('file', 100);
            $table->timestamps();
        });

        Schema::create('puskesmas', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('wilayah_id', 10);
            $table->string('nama', 50);
            $table->string('alamat', 250);
            $table->string('lat', 50);
            $table->string('lng', 50);
            $table->string('jenis', 15);
            $table->string('tahun', 5);
            $table->timestamps();

            $table->foreign('wilayah_id')->references('id')->on('wilayah')->onDelete('cascade');
        });

        Schema::create('petugas', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('user_id', 10);
            $table->string('puskesmas_id', 10);
            $table->string('nip', 50);
            $table->string('nama', 50);
            $table->string('foto', 100)->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('puskesmas_id')->references('id')->on('puskesmas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('petugas', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
            $table->dropForeign(['puskesmas_id']);
            $table->dropColumn('puskesmas_id');
        });
        Schema::table('puskesmas', function (Blueprint $table) {
            $table->dropForeign(['wilayah_id']);
            $table->dropColumn('wilayah_id');
        });
        Schema::dropIfExists('puskesmas');
        Schema::dropIfExists('wilayah');
        Schema::dropIfExists('petugas');
        Schema::dropIfExists('users');
    }
}
