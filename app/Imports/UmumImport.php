<?php

namespace App\Imports;

use App\Models\Bidan;
use App\Models\Gigi;
use App\Models\Kunjungan;
use App\Models\Perawat;
use App\Models\Umum;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Str;

class UmumImport implements ToCollection, WithHeadingRow, SkipsOnError, SkipsOnFailure
{
    use Importable, SkipsErrors, SkipsFailures;

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        // Validator::make($rows->toArray(), [
        //     '*.email' => ['email', 'unique:users,email'],
        // ])->validate();
        foreach ($rows as $row) {
            Umum::create([
                'puskesmas_id' => $row['puskesmas'],
                'jumlah' => $row['umum'],
                'tahun' => $row['tahun'],
                'hash' => Str::random(32)
            ]);

            Gigi::create([
                'puskesmas_id' => $row['puskesmas'],
                'jumlah' => $row['gigi'],
                'tahun' => $row['tahun'],
                'hash' => Str::random(32)
            ]);

            Bidan::create([
                'puskesmas_id' => $row['puskesmas'],
                'jumlah' => $row['bidan'],
                'tahun' => $row['tahun'],
                'hash' => Str::random(32)
            ]);

            Perawat::create([
                'puskesmas_id' => $row['puskesmas'],
                'jumlah' => $row['perawat'],
                'tahun' => $row['tahun'],
                'hash' => Str::random(32)
            ]);

            Kunjungan::create([
                'puskesmas_id' => $row['puskesmas'],
                'rawat_jalan' => $row['jalan'],
                'rawat_inap' => $row['inap'],
                'tahun' => $row['tahun'],
                'hash' => Str::random(32)
            ]);
        }
    }
}
