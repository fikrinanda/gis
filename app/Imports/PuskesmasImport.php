<?php

namespace App\Imports;

use App\Models\Puskesmas;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PuskesmasImport implements ToCollection, WithHeadingRow, SkipsOnError, SkipsOnFailure
{
    use Importable, SkipsErrors, SkipsFailures;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        // Validator::make($rows->toArray(), [
        //     '*.email' => ['email', 'unique:users,email'],
        // ])->validate();

        foreach ($rows as $row) {
            Puskesmas::create([
                'id' => $row['id'],
                'wilayah_id' => $row['wilayah'],
                'nama' => $row['nama'],
                'alamat' => $row['alamat'],
                'lat' => $row['lat'],
                'lng' => $row['lng'],
                'jenis' => $row['jenis'],
                'tahun' => $row['tahun']
            ]);
        }
    }
}
