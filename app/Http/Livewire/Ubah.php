<?php

namespace App\Http\Livewire;

use App\Models\Petugas;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Ubah extends Component
{
    use WithFileUploads;
    public $username;
    public $foto;
    public $foto2;
    protected $listeners = ['berhasil'];

    public function mount()
    {
        if (auth()->user()->level == 'Admin') {
            $user = User::find(auth()->user()->id);
            $this->username = $user->username;
        } else if (auth()->user()->level == 'Petugas') {
            $user = User::find(auth()->user()->id);
            $this->username = $user->username;
            $this->foto = $user->petugas->foto;
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'username' => 'required|min:8|max:24|unique:users,username,' . auth()->id(),
            // 'foto' => 'required|image|max:5000',
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'username' => 'required|min:8|max:24|unique:users,username,' . auth()->id(),
        ]);

        if ($this->foto2) {
            Storage::disk('public')->delete($this->foto);
            $foto2 = $this->foto2->store('images/petugas/foto', 'public');
        } else {
            $foto2 = $this->foto ?? null;
        }

        User::where('id', auth()->user()->id)->update([
            'username' => $this->username,
        ]);

        if (auth()->user()->level == 'Petugas') {
            Petugas::where('id', auth()->user()->petugas->id)->update([
                'foto' => $foto2
            ]);
        }

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Berhasil ubah profil",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/profil');
    }

    public function render()
    {
        if (auth()->user()->level == 'Admin') {
            return view('livewire.ubah')->extends('layouts.admin', ['title' => 'Ubah Profil'])->section('content');
        } else if (auth()->user()->level == 'Petugas') {
            return view('livewire.ubah')->extends('layouts.petugas', ['title' => 'Ubah Profil'])->section('content');
        }
    }
}
