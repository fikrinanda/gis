<?php

namespace App\Http\Livewire\Gigi;

use App\Models\Gigi;
use App\Models\Puskesmas;
use Carbon\Carbon;
use Livewire\Component;
use Illuminate\Support\Str;

class Tambah extends Component
{
    public $jumlah;
    public $tgl;
    public $tahun;
    public $max;
    protected $listeners = ['berhasil'];

    public function updated($field)
    {
        $this->validateOnly($field, [
            'jumlah' => 'required|numeric|min:0',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);
    }

    public function mount()
    {
        $puskesmas = Puskesmas::find(auth()->user()->petugas->puskesmas_id);
        $this->tgl = $puskesmas->tahun;
        $this->max = Carbon::now()->format('Y');
    }

    public function tambah()
    {
        $this->validate([
            'jumlah' => 'required|numeric|min:0',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);

        if (Gigi::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $this->tahun)->exists()) {
            $this->showAlert();
        } else {
            Gigi::create([
                'puskesmas_id' => auth()->user()->petugas->puskesmas_id,
                'jumlah' => $this->jumlah,
                'tahun' => $this->tahun,
                'hash' => Str::random(32)
            ]);

            $this->showModal();
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Jumlah dokter gigi tahun $this->tahun berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/gigi/data');
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'warning',
            'title'   => "Data tahun $this->tahun sudah ada",
            'timeout' => 3000
        ]);
    }

    public function render()
    {
        return view('livewire.gigi.tambah')->extends('layouts.petugas', ['title' => 'Tambah Jumlah Dokter Gigi'])->section('content');
    }
}
