<?php

namespace App\Http\Livewire\Gigi;

use App\Models\Gigi;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($tahun)
    {
        $umum = Gigi::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $tahun)->first();
        $this->showConfirmation($umum->tahun, $umum->puskesmas->nama);
    }

    public function hancur($id)
    {
        $umum = Gigi::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $id)->first()->puskesmas->nama;
        Gigi::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $id)->delete();
        $this->showModal("jumlah dokter gigi puskesmas $umum tahun $id");
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data $nama berhasil dihapus",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'type'    => 'success',
            'title'   => 'This is a success alert!!',
            'timeout' => 5000
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus jumlah dokter gigi puskesmas $nama tahun $id?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $gigi = Gigi::where('puskesmas_id', auth()->user()->petugas->puskesmas->id)->paginate($this->perPage);
        return view('livewire.gigi.data', compact(['gigi']))->extends('layouts.petugas', ['title' => 'Jumlah Dokter Gigi'])->section('content');
    }
}
