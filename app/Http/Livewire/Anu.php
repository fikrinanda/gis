<?php

namespace App\Http\Livewire;

use App\Models\Bidan;
use App\Models\Gigi;
use App\Models\Kunjungan;
use App\Models\Perawat;
use App\Models\Poli;
use App\Models\Puskesmas;
use App\Models\Umum;
use App\Models\Wilayah;
use Carbon\Carbon;
use Livewire\Component;

class Anu extends Component
{
    public $tahun;
    public $t;
    public $pl;
    public $nm;
    public $nama;

    public function mount($thn = null, $nama = null)
    {
        if ($nama == null) {
            $this->nm = null;
        } else {
            $this->nm = $nama;
        }

        if ($thn == null) {
            $this->t = Umum::latest('updated_at')->first()->tahun;
        } else {
            if ((isset(Umum::where('tahun', $thn)->first()->jumlah)) || (isset(Gigi::where('tahun', $thn)->first()->jumlah)) || (isset(Perawat::where('tahun', $thn)->first()->jumlah)) || (isset(Bidan::where('tahun', $thn)->first()->jumlah)) || (isset(Kunjungan::where('tahun', $thn)->first()->jumlah))) {
                $this->t = $thn;
            } else {
                abort('404');
            }
        }
        // dd($this->t);
    }

    public function updated()
    {
        if ($this->tahun == Umum::latest('updated_at')->first()->tahun) {
            return redirect()->to('/');
        } else if ($this->tahun == null) {
            return redirect()->to("/$this->t/$this->pl");
        } else {
            return redirect()->to("/$this->tahun/$this->pl");
        }
    }

    public function render()
    {
        $wilayah = Wilayah::get();
        $puskesmas = Puskesmas::where('tahun', '<=', $this->t)->get();
        $poli = Poli::select('poli')->where('tahun', '<=', $this->t)->groupBy('poli')->get();
        $aduh = Poli::where('tahun', '<=', $this->t)->where('poli', 'like', '%' . $this->nm . '%')->get();
        $query = Umum::distinct()->get(['tahun']);
        return view('livewire.anu', compact(['wilayah', 'puskesmas', 'poli', 'query', 'aduh']))->extends('layouts.anu', ['title' => 'Sistem Informasi Geografis', 'tahun' => $this->t])->section('content');
    }
}
