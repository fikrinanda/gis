<?php

namespace App\Http\Livewire\Umum;

use App\Models\Puskesmas;
use App\Models\Umum;
use Carbon\Carbon;
use Livewire\Component;

class Ubah extends Component
{
    public $jumlah;
    public $tgl;
    public $tahun;
    public $t;
    public $max;
    public $hash;
    protected $listeners = ['berhasil'];

    public function mount($tahun)
    {
        $this->max = Carbon::now()->format('Y');
        $puskesmas = Puskesmas::find(auth()->user()->petugas->puskesmas_id);
        $this->tgl = $puskesmas->tahun;
        $test = Umum::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $tahun)->exists();
        if ($test) {
            $umum = Umum::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $tahun)->first();
            $this->jumlah = $umum->jumlah;
            $this->t = $tahun;
            $this->hash = $umum->hash;
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'jumlah' => 'required|numeric|min:0',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'jumlah' => 'required|numeric|min:0',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);

        if (Umum::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $this->tahun)->where('hash', '!=', $this->hash)->exists()) {
            $this->showAlert();
        } else {
            Umum::where('hash', $this->hash)->update([
                'jumlah' => $this->jumlah,
                'tahun' => $this->tahun
            ]);

            $this->showModal();
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Jumlah dokter umum berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/umum/data');
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'warning',
            'title'   => "Data tahun $this->tahun sudah ada",
            'timeout' => 3000
        ]);
    }

    public function render()
    {
        return view('livewire.umum.ubah')->extends('layouts.petugas', ['title' => 'Ubah Jumlah Dokter Umum'])->section('content');
    }
}
