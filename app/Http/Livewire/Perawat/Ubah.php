<?php

namespace App\Http\Livewire\Perawat;

use App\Models\Perawat;
use App\Models\Puskesmas;
use Carbon\Carbon;
use Livewire\Component;

class Ubah extends Component
{
    public $jumlah;
    public $tgl;
    public $tahun;
    public $t;
    public $max;
    public $hash;
    protected $listeners = ['berhasil'];

    public function mount($tahun)
    {
        $this->max = Carbon::now()->format('Y');
        $puskesmas = Puskesmas::find(auth()->user()->petugas->puskesmas_id);
        $this->tgl = $puskesmas->tahun;
        $test = Perawat::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $tahun)->exists();
        if ($test) {
            $umum = Perawat::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $tahun)->first();
            $this->jumlah = $umum->jumlah;
            $this->t = $tahun;
            $this->hash = $umum->hash;
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'jumlah' => 'required|numeric|min:0',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'jumlah' => 'required|numeric|min:0',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);

        if (Perawat::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $this->tahun)->where('hash', '!=', $this->hash)->exists()) {
            $this->showAlert();
        } else {
            Perawat::where('hash', $this->hash)->update([
                'jumlah' => $this->jumlah,
                'tahun' => $this->tahun
            ]);

            $this->showModal();
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Jumlah perawat berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/perawat/data');
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'warning',
            'title'   => "Data tahun $this->tahun sudah ada",
            'timeout' => 3000
        ]);
    }

    public function render()
    {
        return view('livewire.perawat.ubah')->extends('layouts.petugas', ['title' => 'Ubah Jumlah Perawat'])->section('content');
    }
}
