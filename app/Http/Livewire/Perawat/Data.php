<?php

namespace App\Http\Livewire\Perawat;

use App\Models\Perawat;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($tahun)
    {
        $umum = Perawat::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $tahun)->first();
        $this->showConfirmation($umum->tahun, $umum->puskesmas->nama);
    }

    public function hancur($id)
    {
        $umum = Perawat::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $id)->first()->puskesmas->nama;
        Perawat::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $id)->delete();
        $this->showModal("jumlah perawat puskesmas $umum tahun $id");
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data $nama berhasil dihapus",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'type'    => 'success',
            'title'   => 'This is a success alert!!',
            'timeout' => 5000
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus jumlah perawat puskesmas $nama tahun $id?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $perawat = Perawat::where('puskesmas_id', auth()->user()->petugas->puskesmas->id)->paginate($this->perPage);
        return view('livewire.perawat.data', compact(['perawat']))->extends('layouts.petugas', ['title' => 'Jumlah Perawat'])->section('content');
    }
}
