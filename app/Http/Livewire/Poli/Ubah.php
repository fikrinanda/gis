<?php

namespace App\Http\Livewire\Poli;

use App\Models\Poli;
use App\Models\Puskesmas;
use Carbon\Carbon;
use Livewire\Component;

class Ubah extends Component
{
    public $i;
    public $nama;
    public $poli;
    public $tgl;
    public $tahun;
    public $t;
    public $max;
    public $hash;
    protected $listeners = ['berhasil'];

    public function mount($hash)
    {
        // $this->max = Carbon::now()->format('Y');
        // $puskesmas = Puskesmas::find(auth()->user()->petugas->puskesmas_id);
        $test = Poli::where('hash', $hash)->exists();
        if ($test) {
            $umum = Poli::where('hash', $hash)->first();
            $this->hash = $umum->hash;
            $this->tgl = $umum->puskesmas->tahun;
            $this->poli = $umum->poli;
            $this->i = $umum->puskesmas_id;
            $this->tahun = $umum->tahun;
            $this->max = Carbon::now()->format('Y');
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'poli' => 'required',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'poli' => 'required',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);

        Poli::where('hash', $this->hash)->update([
            'poli' => $this->poli,
            'tahun' => $this->tahun
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Poli berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/puskesmas/data');
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'warning',
            'title'   => "Data tahun $this->tahun sudah ada",
            'timeout' => 3000
        ]);
    }

    public function render()
    {
        return view('livewire.poli.ubah')->extends('layouts.admin', ['title' => 'Ubah Poli'])->section('content');
    }
}
