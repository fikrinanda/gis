<?php

namespace App\Http\Livewire\Poli;

use App\Models\Poli;
use App\Models\Puskesmas;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Livewire\Component;

class Tambah extends Component
{
    public $i;
    public $nama;
    public $poli;
    public $tahun;
    public $tgl;
    public $max;
    protected $listeners = ['berhasil'];

    public function mount($nama)
    {
        $puskesmas = Puskesmas::where('nama', $nama)->first();
        if ($puskesmas) {
            $this->i = $puskesmas->id;
            $this->tgl = $puskesmas->tahun;
            $this->max = Carbon::now()->format('Y');
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'poli' => 'required',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'poli' => 'required',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);

        Poli::create([
            'puskesmas_id' => $this->i,
            'hash' => Str::random(32),
            'poli' => $this->poli,
            'tahun' => $this->tahun,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Poli puskesmas $this->nama berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("/puskesmas/lihat/$this->nama");
    }

    public function render()
    {
        return view('livewire.poli.tambah')->extends('layouts.admin', ['title' => 'Tambah Poli', 'h2' => 'Tambah Wilayah'])->section('content');
    }
}
