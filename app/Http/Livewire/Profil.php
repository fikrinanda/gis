<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class Profil extends Component
{
    public $user;

    public function mount()
    {
        $this->user = User::find(auth()->user()->id);
    }

    public function render()
    {
        if (auth()->user()->level == 'Admin') {
            return view('livewire.profil')->extends('layouts.admin', ['title' => 'Profil'])->section('content');
        } else if (auth()->user()->level == 'Petugas') {
            return view('livewire.profil')->extends('layouts.petugas', ['title' => 'Profil'])->section('content');
        }
    }
}
