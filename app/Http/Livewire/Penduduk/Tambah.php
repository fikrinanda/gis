<?php

namespace App\Http\Livewire\Penduduk;

use App\Models\Penduduk;
use App\Models\Wilayah;
use Carbon\Carbon;
use Livewire\Component;
use Illuminate\Support\Str;

class Tambah extends Component
{
    public $jumlah;
    public $tgl;
    public $tahun;
    public $max;
    public $wilayah;
    protected $listeners = ['berhasil'];

    public function updated($field)
    {
        $this->validateOnly($field, [
            'wilayah' => 'required',
            'jumlah' => 'required|numeric',
            'tahun' => "required|numeric|max:$this->max"
        ]);
    }

    public function mount()
    {
        $this->max = Carbon::now()->format('Y');
    }

    public function tambah()
    {
        $this->validate([
            'wilayah' => 'required',
            'jumlah' => 'required|numeric',
            'tahun' => "required|numeric|max:$this->max"
        ]);

        if (Penduduk::where('wilayah_id', $this->wilayah)->where('tahun', $this->tahun)->exists()) {
            $this->showAlert();
        } else {
            Penduduk::create([
                'wilayah_id' => $this->wilayah,
                'jumlah' => $this->jumlah,
                'tahun' => $this->tahun,
                'hash' => Str::random(32)
            ]);

            $this->showModal();
        }
    }

    public function showModal()
    {
        $nama = Wilayah::find($this->wilayah)->nama;
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Jumlah penduduk tahun $this->tahun wilayah $nama berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/penduduk/data');
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'warning',
            'title'   => "Data tahun $this->tahun sudah ada",
            'timeout' => 3000
        ]);
    }

    public function render()
    {
        $wly = Wilayah::get();
        return view('livewire.penduduk.tambah', compact(['wly']))->extends('layouts.admin', ['title' => 'Tambah Jumlah Penduduk'])->section('content');
    }
}
