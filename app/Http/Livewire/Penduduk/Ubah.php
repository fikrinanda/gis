<?php

namespace App\Http\Livewire\Penduduk;

use App\Models\Penduduk;
use App\Models\Wilayah;
use Carbon\Carbon;
use Livewire\Component;

class Ubah extends Component
{
    public $wilayah_id;
    public $tahun;
    public $wilayah;
    public $jumlah;
    public $max;
    public $hash;
    protected $listeners = ['berhasil'];

    public function mount($wilayah_id, $tahun)
    {
        if (Penduduk::where('wilayah_id', $wilayah_id)->where('tahun', $tahun)->exists()) {
            $penduduk = Penduduk::where('wilayah_id', $wilayah_id)->where('tahun', $tahun)->first();
            $this->wilayah = $penduduk->wilayah_id;
            $this->tahun = $penduduk->tahun;
            $this->jumlah = $penduduk->jumlah;
            $this->hash = $penduduk->hash;
            $this->max = Carbon::now()->format('Y');
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'wilayah' => 'required',
            'jumlah' => 'required|numeric',
            'tahun' => "required|numeric|max:$this->max"
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'wilayah' => 'required',
            'jumlah' => 'required|numeric',
            'tahun' => "required|numeric|max:$this->max"
        ]);

        if (Penduduk::where('wilayah_id', $this->wilayah)->where('tahun', $this->tahun)->where('hash', '!=', $this->hash)->exists()) {
            $this->showAlert();
        } else {
            Penduduk::where('hash', $this->hash)->update([
                'wilayah_id' => $this->wilayah,
                'jumlah' => $this->jumlah,
                'tahun' => $this->tahun
            ]);

            $this->showModal();
        }
    }

    public function showModal()
    {
        $nama = Wilayah::find($this->wilayah)->nama;
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Jumlah penduduk tahun $this->tahun wilayah $nama berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/penduduk/data');
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'warning',
            'title'   => "Data tahun $this->tahun sudah ada",
            'timeout' => 3000
        ]);
    }

    public function render()
    {
        $wly = Wilayah::get();
        return view('livewire.penduduk.ubah', compact(['wly']))->extends('layouts.admin', ['title' => 'Ubah Jumlah Penduduk'])->section('content');
    }
}
