<?php

namespace App\Http\Livewire\Penduduk;

use App\Models\Penduduk;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($wilayah_id, $tahun)
    {
        $umum = Penduduk::where('wilayah_id', $wilayah_id)->where('tahun', $tahun)->first();
        $this->showConfirmation($umum->wilayah_id, $umum->tahun, $umum->wilayah->nama);
    }

    public function hancur($wilayah_id, $tahun)
    {
        $umum = Penduduk::where('wilayah_id', $wilayah_id)->where('tahun', $tahun)->first()->wilayah->nama;
        Penduduk::where('wilayah_id', $wilayah_id)->where('tahun', $tahun)->delete();
        $this->showModal("jumlah penduduk wilayah $umum tahun $tahun");
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data $nama berhasil dihapus",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'type'    => 'success',
            'title'   => 'This is a success alert!!',
            'timeout' => 5000
        ]);
    }

    public function showConfirmation($wilayah_id, $tahun, $nama)
    {
        $this->emit("swal:confirm2", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus jumlah penduduk wilayah $nama tahun $tahun?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $wilayah_id, // optional, send params to success confirmation
            'params2'      => $tahun, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $penduduk = Penduduk::paginate($this->perPage);
        return view('livewire.penduduk.data', compact(['penduduk']))->extends('layouts.admin', ['title' => 'Data Jumlah Penduduk'])->section('content');
    }
}
