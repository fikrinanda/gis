<?php

namespace App\Http\Livewire\Kunjungan;

use App\Models\Kunjungan;
use App\Models\Puskesmas;
use Carbon\Carbon;
use Livewire\Component;
use Illuminate\Support\Str;

class Tambah extends Component
{
    public $rawat_jalan;
    public $rawat_inap;
    public $tgl;
    public $tahun;
    public $max;
    public $d = "";
    protected $listeners = ['berhasil'];

    public function updated($field)
    {
        $this->validateOnly($field, [
            'rawat_jalan' => 'required|numeric|min:0',
            'rawat_inap' => 'required|numeric|min:0',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);
    }

    public function mount()
    {
        $puskesmas = Puskesmas::find(auth()->user()->petugas->puskesmas_id);
        $this->tgl = $puskesmas->tahun;
        $this->max = Carbon::now()->format('Y');
        if (auth()->user()->petugas->puskesmas->jenis != 'Rawat Inap') {
            $this->rawat_inap = 0;
            $this->d = "disabled";
        }
    }

    public function tambah()
    {
        $this->validate([
            'rawat_jalan' => 'required|numeric|min:0',
            'rawat_inap' => 'required|numeric|min:0',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);

        if (Kunjungan::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $this->tahun)->exists()) {
            $this->showAlert();
        } else {
            Kunjungan::create([
                'puskesmas_id' => auth()->user()->petugas->puskesmas_id,
                'rawat_jalan' => $this->rawat_jalan,
                'rawat_inap' => $this->rawat_jalan,
                'tahun' => $this->tahun,
                'hash' => Str::random(32)
            ]);

            $this->showModal();
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Jumlah kunjungan tahun $this->tahun berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/kunjungan/data');
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'warning',
            'title'   => "Data tahun $this->tahun sudah ada",
            'timeout' => 3000
        ]);
    }

    public function render()
    {
        return view('livewire.kunjungan.tambah')->extends('layouts.petugas', ['title' => 'Tambah Jumlah Kunjungan'])->section('content');
    }
}
