<?php

namespace App\Http\Livewire\Kunjungan;

use App\Models\Kunjungan;
use App\Models\Puskesmas;
use Carbon\Carbon;
use Livewire\Component;

class Ubah extends Component
{
    public $rawat_jalan;
    public $rawat_inap;
    public $tgl;
    public $tahun;
    public $t;
    public $max;
    public $hash;
    protected $listeners = ['berhasil'];

    public function mount($tahun)
    {
        $this->max = Carbon::now()->format('Y');
        $puskesmas = Puskesmas::find(auth()->user()->petugas->puskesmas_id);
        $this->tgl = $puskesmas->tahun;
        $test = Kunjungan::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $tahun)->exists();
        if ($test) {
            $umum = Kunjungan::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $tahun)->first();
            $this->rawat_inap = $umum->rawat_inap;
            $this->rawat_jalan = $umum->rawat_jalan;
            $this->t = $tahun;
            $this->hash = $umum->hash;
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'rawat_jalan' => 'required|numeric|min:0',
            'rawat_inap' => 'required|numeric|min:0',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'rawat_jalan' => 'required|numeric|min:0',
            'rawat_inap' => 'required|numeric|min:0',
            'tahun' => "required|numeric|min:$this->tgl|max:$this->max"
        ]);

        if (Kunjungan::where('puskesmas_id', auth()->user()->petugas->puskesmas_id)->where('tahun', $this->tahun)->where('hash', '!=', $this->hash)->exists()) {
            $this->showAlert();
        } else {
            Kunjungan::where('hash', $this->hash)->update([
                'rawat_jalan' => $this->rawat_jalan,
                'rawat_inap' => $this->rawat_jalan,
                'tahun' => $this->tahun,
            ]);

            $this->showModal();
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Jumlah kunjungan berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/kunjungan/data');
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'warning',
            'title'   => "Data tahun $this->tahun sudah ada",
            'timeout' => 3000
        ]);
    }
    public function render()
    {
        return view('livewire.kunjungan.ubah')->extends('layouts.petugas', ['title' => 'Ubah Jumlah Kunjungan'])->section('content');
    }
}
