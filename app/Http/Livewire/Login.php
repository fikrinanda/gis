<?php

namespace App\Http\Livewire;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Login extends Component
{
    public $username;
    public $password;
    public $tahun;

    public function mount()
    {
        $chae = Carbon::now()->format('m');
        if ($chae > 7) {
            $this->tahun = Carbon::now()->format('Y') - 1;
        } else if ($chae < 7) {
            $this->tahun = Carbon::now()->format('Y') - 2;
        }
    }

    public function login()
    {
        $this->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = User::whereUsername($this->username)->first();
        if ($user && Hash::check($this->password, $user->password)) {
            Auth::login($user);
            if ($user->level == 'Admin') {
                return redirect()->intended("/admin");
            } else if ($user->level == 'Petugas') {
                return redirect()->intended('/petugas');
            } else {
                return back()->with('error', 'Username / Password Salah')->with($this->showModal());
            }
        } else {
            return back()->with('error', 'Username / Password Salah')->with($this->showModal());
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'error',
            'title' => 'Gagal!!!',
            'text'  => "Username / Password Salah",
        ]);
    }

    public function render()
    {
        return view('livewire.login')->extends('layouts.login', ['title' => 'Login'])->section('content');
    }
}
