<?php

namespace App\Http\Livewire\Petugas;

use App\Models\Petugas;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($id)
    {
        $user = User::find($id);
        $this->showConfirmation($user->id, $user->petugas->nama);
    }

    public function hancur($id)
    {
        $user = User::find($id);
        $nama = $user->petugas->nama;
        $petugas = Petugas::where('user_id', $id)->first();
        Storage::disk('public')->delete($petugas->foto);
        $user->delete();
        $this->showModal($nama);
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Petugas $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Petugas $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $petugas = Petugas::selectRaw('petugas.*')->join('users', 'users.id', '=', 'petugas.user_id')->where('nama', 'like', '%' . $this->search . '%')->orWhere('username', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        return view('livewire.petugas.data', compact(['petugas']))->extends('layouts.admin', ['title' => 'Data Petugas', 'h2' => 'Data Puskesmas'])->section('content');
    }
}
