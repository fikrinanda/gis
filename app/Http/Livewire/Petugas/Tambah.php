<?php

namespace App\Http\Livewire\Petugas;

use App\Models\Petugas;
use App\Models\Puskesmas;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithFileUploads;

class Tambah extends Component
{
    use WithFileUploads;

    public $username;
    public $password;
    public $puskesmas_id;
    public $nip;
    public $nama;
    public $foto;
    protected $listeners = ['berhasil'];

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nip' => 'required|numeric',
            'username' => 'required|min:8|max:24|unique:users,username',
            'password' => 'required|min:8|max:24',
            'puskesmas_id' => 'required',
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'foto' => 'required|image|max:5000',
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nip' => 'required|numeric',
            'username' => 'required|min:8|max:24|unique:users,username',
            'password' => 'required|min:8|max:24',
            'puskesmas_id' => 'required',
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'foto' => 'required|image|max:5000',
        ]);

        $foto = $this->foto->store('images/petugas/foto', 'public');

        $x = User::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "US" . sprintf("%04s", $y);

        User::create([
            'id' => $z,
            'username' => $this->username,
            'password' => Hash::make($this->password),
            'level' => 'Petugas',
        ]);

        $user = User::where('username', $this->username)->first();

        $x2 = Petugas::max('id');
        $y2 = (int) substr($x2, 2, 4);
        $y2++;
        $z2 = "PG" . sprintf("%04s", $y2);

        Petugas::create([
            'id' => $z2,
            'user_id' => $user->id,
            'puskesmas_id' => $this->puskesmas_id,
            'nip' => $this->nip,
            'nama' => $this->nama,
            'foto' => $foto,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Petugas $this->nama berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/petugas/data');
    }

    public function render()
    {
        $puskesmas = Puskesmas::get();
        return view('livewire.petugas.tambah', compact(['puskesmas']))->extends('layouts.admin', ['title' => 'Data Petugas', 'h2' => 'Data Puskesmas'])->section('content');
    }
}
