<?php

namespace App\Http\Livewire\Petugas;

use App\Models\Petugas;
use App\Models\Puskesmas;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Ubah extends Component
{
    use WithFileUploads;

    public $puskesmas_id;
    public $nip;
    public $nama;
    public $foto;
    public $foto2;
    public $uname;
    public $password;
    public $i;
    public $i2;
    protected $listeners = ['berhasil'];

    public function mount($username)
    {
        $user = User::where('username', $username)->first();

        if ($user) {
            $petugas = Petugas::where('user_id', $user->id)->first();
            if ($petugas) {
                $this->i = $petugas->id;
                $this->i2 = $petugas->user->id;
                $this->puskesmas_id = $petugas->puskesmas->id;
                $this->nip = $petugas->nip;
                $this->nama = $petugas->nama;
                $this->foto = $petugas->foto;
                $this->uname = $petugas->user->username;
            } else {
                abort('404');
            }
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nip' => 'required|numeric',
            'puskesmas_id' => 'required',
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'uname' => 'required|min:8|max:24|unique:users,username,' . $this->i2,
            // 'password' => 'min:8|max:24',
            // 'foto' => 'required|image|max:5000',
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'nip' => 'required|numeric',
            'puskesmas_id' => 'required',
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'uname' => 'required|min:8|max:24|unique:users,username,' . $this->i2,
            // 'password' => 'required|min:8|max:24',
            // 'foto' => 'required|image|max:5000',
        ]);

        if ($this->foto2) {
            Storage::disk('public')->delete($this->foto);
            $foto2 = $this->foto2->store('images/petugas/foto', 'public');
        } else {
            $foto2 = $this->foto ?? null;
        }

        Petugas::where('id', $this->i)->update([
            'puskesmas_id' => $this->puskesmas_id,
            'nip' => $this->nip,
            'nama' => $this->nama,
            'foto' => $foto2,
        ]);

        if ($this->password == null) {
            User::where('id', $this->i2)->update([
                'username' => $this->uname
            ]);
        } else {
            User::where('id', $this->i2)->update([
                'username' => $this->uname,
                'password' => Hash::make($this->password)
            ]);
        }

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Petugas $this->nama berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/petugas/data');
    }

    public function render()
    {
        $puskesmas = Puskesmas::get();
        return view('livewire.petugas.ubah', compact(['puskesmas']))->extends('layouts.admin', ['title' => 'Ubah Petugas'])->section('content');
    }
}
