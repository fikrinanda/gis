<?php

namespace App\Http\Livewire\Petugas;

use App\Models\User;
use Livewire\Component;

class Lihat extends Component
{
    public $user;

    public function mount($username)
    {
        $test = User::where('username', $username)->where('level', 'Petugas')->exists();
        if ($test) {
            $this->user = User::where('username', $username)->where('level', 'Petugas')->first();
        } else {
            abort('404');
        }
    }

    public function render()
    {
        return view('livewire.petugas.lihat')->extends('layouts.admin', ['title' => 'Lihat Petugas', 'h2' => 'Dashboard'])->section('content');
    }
}
