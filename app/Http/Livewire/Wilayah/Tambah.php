<?php

namespace App\Http\Livewire\Wilayah;

use App\Models\Wilayah;
use Livewire\Component;
use Livewire\WithFileUploads;

class Tambah extends Component
{
    use WithFileUploads;

    public $nama;
    public $file;
    public $tmp;
    protected $listeners = ['berhasil'];

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'file' => 'required',
        ]);
    }

    public function lihat()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'file' => 'required',
        ]);

        $file = $this->file->storeAs('file/wilayah', \Str::random(40) . '.' . $this->file->getClientOriginalExtension(), 'public');

        $this->tmp = $file;
    }

    public function tambah()
    {
        $x = Wilayah::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "WL" . sprintf("%04s", $y);

        Wilayah::create([
            'id' => $z,
            'nama' => $this->nama,
            'file' => $this->tmp,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data wilayah $this->nama berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/wilayah/data');
    }

    public function render()
    {
        return view('livewire.wilayah.tambah')->extends('layouts.admin', ['title' => 'Tambah Wilayah', 'h2' => 'Tambah Wilayah'])->section('content');
    }
}
