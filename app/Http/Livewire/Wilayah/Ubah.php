<?php

namespace App\Http\Livewire\Wilayah;

use App\Models\Wilayah;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Ubah extends Component
{
    use WithFileUploads;

    public $nama;
    public $file;
    public $file2;
    public $tmp;
    public $i;
    protected $listeners = ['berhasil'];

    public function mount($nama)
    {
        $wilayah = Wilayah::where('nama', $nama)->first();
        if ($wilayah) {
            $this->i = $wilayah->id;
            $this->nm = $wilayah->nama;
            $this->file = $wilayah->file;
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            // 'foto' => 'required|image|max:5000',
        ]);
    }

    public function lihat()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
        ]);

        if ($this->file2) {
            Storage::disk('public')->delete($this->file);
            $file2 = $this->file2->storeAs('file/wilayah', \Str::random(40) . '.' . $this->file2->getClientOriginalExtension(), 'public');
        } else {
            $file2 = $this->file ?? null;
        }

        $this->tmp = $file2;
    }

    public function ubah()
    {
        Wilayah::where('id', $this->i)->update([
            'nama' => $this->nama,
            'file' => $this->tmp,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Wilayah $this->nama berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/wilayah/data');
    }

    public function render()
    {
        return view('livewire.wilayah.ubah')->extends('layouts.admin', ['title' => 'Ubah Wilayah'])->section('content');
    }
}
