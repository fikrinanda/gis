<?php

namespace App\Http\Livewire\Wilayah;

use App\Models\Wilayah;
use Livewire\Component;

class Lihat extends Component
{
    public $nm;
    public $file;

    public function mount($nama)
    {
        $wilayah = Wilayah::where('nama', $nama)->first();
        if ($wilayah) {
            $this->nm = $wilayah->nama;
            $this->file = $wilayah->file;
        } else {
            abort('404');
        }
    }

    public function render()
    {
        return view('livewire.wilayah.lihat')->extends('layouts.admin', ['title' => 'Lihat Wilayah', 'h2' => 'Lihat Wilayah'])->section('content');
    }
}
