<?php

namespace App\Http\Livewire\Puskesmas;

use App\Models\Poli;
use App\Models\Puskesmas;
use App\Models\Wilayah;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Livewire\Component;

class Tambah extends Component
{
    public $wilayah;
    public $nama;
    public $alamat;
    public $latitude;
    public $longitude;
    public $jenis;
    public $lat;
    public $lng;
    public $a;
    public $tahun;
    public $t;
    public $poli = [];
    protected $listeners = ['berhasil', 'getLatitudeForInput', 'getLongitudeForInput'];

    public function mount()
    {
        $this->t = Carbon::now()->format('Y');
        $this->poli = [
            []
        ];
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|min:3',
            'alamat' => 'required|min:3',
            'wilayah' => 'required',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'jenis' => 'required',
            'tahun' => "required|numeric|max:$this->t",
            'poli.*' => 'required'
        ]);
    }

    public function getLatitudeForInput($value)
    {
        if (!is_null($value))
            $this->latitude = $value;
    }

    public function getLongitudeForInput($value)
    {
        if (!is_null($value))
            $this->longitude = $value;
    }

    public function add()
    {
        $this->poli[] = [];
    }

    public function remove($index)
    {
        if ((count($this->poli) > 1)) {
            unset($this->poli[$index]);
            $this->poli = array_values($this->poli);
        }
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required|min:3',
            'alamat' => 'required|min:3',
            'wilayah' => 'required',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'jenis' => 'required',
            'tahun' => "required|numeric|max:$this->t",
            'poli.*' => 'required'
        ]);

        $x = Puskesmas::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "PK" . sprintf("%04s", $y);

        Puskesmas::create([
            'id' => $z,
            'wilayah_id' => $this->wilayah,
            'nama' => $this->nama,
            'alamat' => $this->alamat,
            'lat' => $this->latitude,
            'lng' => $this->longitude,
            'jenis' => $this->jenis,
            'tahun' => $this->tahun
        ]);

        foreach ($this->poli as $data) {
            Poli::create([
                'puskesmas_id' => $z,
                'hash' => Str::random(32),
                'poli' => $data,
                'tahun' => $this->tahun
            ]);
        }

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data puskesmas $this->nama berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/puskesmas/data');
    }

    public function render()
    {
        $wly = Wilayah::get();
        return view('livewire.puskesmas.tambah', compact(['wly']))->extends('layouts.admin', ['title' => 'Tambah Puskesmas', 'h2' => 'Tambah Wilayah'])->section('content');
    }
}
