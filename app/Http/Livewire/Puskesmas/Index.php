<?php

namespace App\Http\Livewire\Puskesmas;

use App\Models\Puskesmas;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($id)
    {
        $wilayah = Puskesmas::find($id);
        $this->showConfirmation($wilayah->id, $wilayah->nama);
    }

    public function hancur($id)
    {
        // $user = User::find($id);
        // $nama = $user->dokter->nama;
        $wilayah = Puskesmas::find($id);
        Storage::disk('public')->delete($wilayah->file);
        $wilayah->delete();
        $this->showModal($wilayah->nama);
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data puskesmas $nama berhasil dihapus",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'type'    => 'success',
            'title'   => 'This is a success alert!!',
            'timeout' => 5000
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus wilayah $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $puskesmas = Puskesmas::where('nama', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        return view('livewire.puskesmas.index', compact(['puskesmas']))->extends('layouts.admin', ['title' => 'Data Puskesmas', 'h2' => 'Data Puskesmas'])->section('content');
    }
}
