<?php

namespace App\Http\Livewire\Puskesmas;

use App\Models\Detail;
use Livewire\Component;

class Data extends Component
{
    public function render()
    {
        $detail = Detail::where('puskesmas_id', auth()->user()->puskesmas_id)->paginate(5);
        return view('livewire.puskesmas.data', compact(['detail']))->extends('layouts.petugas', ['title' => 'Data'])->section('content');;
    }
}
