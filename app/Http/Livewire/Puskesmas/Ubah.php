<?php

namespace App\Http\Livewire\Puskesmas;

use App\Models\Puskesmas;
use App\Models\Wilayah;
use Carbon\Carbon;
use Livewire\Component;

class Ubah extends Component
{
    public $i;
    public $wilayah;
    public $nama;
    public $alamat;
    public $latitude;
    public $longitude;
    public $jenis;
    public $lat;
    public $lng;
    public $a;
    public $tahun;
    public $t;
    protected $listeners = ['berhasil', 'getLatitudeForInput', 'getLongitudeForInput'];

    public function mount($nama)
    {
        $this->t = Carbon::now()->format('Y');
        $puskesmas = Puskesmas::where('nama', $nama)->first();
        if ($puskesmas) {
            $this->i = $puskesmas->id;
            $this->nama = $puskesmas->nama;
            $this->wilayah = $puskesmas->wilayah_id;
            $this->alamat = $puskesmas->alamat;
            $this->latitude = $puskesmas->lat;
            $this->longitude = $puskesmas->lng;
            $this->jenis = $puskesmas->jenis;
            $this->tahun = $puskesmas->tahun;
            $this->a = Wilayah::where('id', $puskesmas->wilayah_id)->first();
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|min:3',
            'alamat' => 'required|min:3',
            'wilayah' => 'required',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'jenis' => 'required',
            'tahun' => "required|numeric|max:$this->t"
        ]);
    }

    public function getLatitudeForInput($value)
    {
        if (!is_null($value))
            $this->latitude = $value;
    }

    public function getLongitudeForInput($value)
    {
        if (!is_null($value))
            $this->longitude = $value;
    }

    public function lihat()
    {
        $this->validate([
            'nama' => 'required|min:3',
            'alamat' => 'required|min:3',
            'wilayah' => 'required',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'jenis' => 'required',
            'tahun' => "required|numeric|max:$this->t"
        ]);

        $this->lat = $this->latitude;
        $this->lng = $this->longitude;
        $this->a = Wilayah::where('id', $this->wilayah)->first();
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required|min:3',
            'alamat' => 'required|min:3',
            'wilayah' => 'required',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'jenis' => 'required',
            'tahun' => "required|numeric|max:$this->t"
        ]);

        Puskesmas::where('id', $this->i)->update([
            'wilayah_id' => $this->wilayah,
            'nama' => $this->nama,
            'alamat' => $this->alamat,
            'lat' => $this->latitude,
            'lng' => $this->longitude,
            'jenis' => $this->jenis,
            'tahun' => $this->tahun
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data puskesmas $this->nama berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/puskesmas/data');
    }

    public function render()
    {
        $wly = Wilayah::get();
        return view('livewire.puskesmas.ubah', compact(['wly']))->extends('layouts.admin', ['title' => 'Ubah Puskesmas'])->section('content');
    }
}
