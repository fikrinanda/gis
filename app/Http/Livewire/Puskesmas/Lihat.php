<?php

namespace App\Http\Livewire\Puskesmas;

use App\Models\Poli;
use App\Models\Puskesmas;
use Livewire\Component;
use Livewire\WithPagination;

class Lihat extends Component
{
    use WithPagination;

    public $nm;
    public $lat;
    public $lng;
    public $file;
    public $i;
    protected $listeners = ['yakin' => 'hancur', 'batal'];
    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function mount($nama)
    {
        $puskesmas = Puskesmas::where('nama', $nama)->first();
        if ($puskesmas) {
            $this->i = $puskesmas->id;
            $this->nm = $puskesmas->nama;
            $this->lat = $puskesmas->lat;
            $this->lng = $puskesmas->lng;
            $this->file = $puskesmas->wilayah->file;
        } else {
            abort('404');
        }
    }

    public function hapus($id)
    {
        $this->showConfirmation($id);
    }

    public function hancur($id)
    {
        Poli::where('hash', $id)->first()->delete();
        $this->showModal($this->nm);
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data poli $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus poli $this->nm?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }


    public function render()
    {
        $poli = Poli::where('puskesmas_id', $this->i)->paginate(5);
        return view('livewire.puskesmas.lihat', compact(['poli']))->extends('layouts.admin', ['title' => 'Lihat Puskesmas', 'h2' => 'Lihat Puskesmas'])->section('content');
    }
}
