<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Logout extends Component
{
    public function mount()
    {
        auth()->logout();
        return redirect('/login');
    }

    public function render()
    {
        if (auth()->user()->level == 'Admin') {
            return view('livewire.logout')->extends('layouts.admin')->section('content');
        } else if (auth()->user()->level == 'Petugas') {
            return view('livewire.logout')->extends('layouts.petugas')->section('content');
        }
    }
}
