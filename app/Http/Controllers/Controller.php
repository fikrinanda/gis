<?php

namespace App\Http\Controllers;

use App\Imports\PendudukImport;
use App\Imports\PuskesmasImport;
use App\Imports\UmumImport;
use App\Imports\WilayahImport;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __invoke(Request $request)
    {
        auth()->logout();
        return redirect('/login');
    }

    public function show()
    {
        return view('index');
    }

    public function wilayah()
    {
        $file = request('file');
        $import = new WilayahImport;
        $import->import($file);
        if ($import->failures()->isNotEmpty()) {
            return back()->withFailures($import->failures());
        }
        return redirect('/import')->withStatus('Berhasil');
    }

    public function puskesmas()
    {
        $file = request('file');
        $import = new PuskesmasImport;
        $import->import($file);
        if ($import->failures()->isNotEmpty()) {
            return back()->withFailures($import->failures());
        }
        return redirect('/import')->withStatus('Berhasil');
    }

    public function umum()
    {
        $file = request('file');
        $import = new UmumImport;
        $import->import($file);
        if ($import->failures()->isNotEmpty()) {
            return back()->withFailures($import->failures());
        }
        return redirect('/import')->withStatus('Berhasil');
    }

    public function penduduk()
    {
        $file = request('file');
        $import = new PendudukImport;
        $import->import($file);
        if ($import->failures()->isNotEmpty()) {
            return back()->withFailures($import->failures());
        }
        return redirect('/import')->withStatus('Berhasil');
    }
}
