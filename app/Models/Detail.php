<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    use HasFactory;

    protected $table = 'detail_puskesmas';
    protected $fillable = ['puskesmas_id', 'jumlah_kunjungan', 'jumlah_dokter', 'jumlah_perawat', 'jumlah_bidan', 'jumlah_posyandu', 'tahun'];
}
