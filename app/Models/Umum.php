<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Umum extends Model
{
    use HasFactory;

    protected $table = 'dokter_umum';
    protected $fillable = ['puskesmas_id', 'jumlah', 'tahun', 'hash'];

    public function puskesmas()
    {
        return $this->hasOne(Puskesmas::class, 'id', 'puskesmas_id');
    }
}
