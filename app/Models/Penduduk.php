<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penduduk extends Model
{
    use HasFactory;

    protected $table = 'penduduk';
    protected $fillable = ['wilayah_id', 'jumlah', 'tahun', 'hash'];

    public function wilayah()
    {
        return $this->hasOne(Wilayah::class, 'id', 'wilayah_id');
    }
}
