<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Poli extends Model
{
    use HasFactory;

    protected $table = "poli";
    protected $fillable = ['puskesmas_id', 'hash', 'poli', 'tahun'];
    public $primaryKey = 'hash';
    public $incrementing = false;

    public function puskesmas()
    {
        return $this->hasOne(Puskesmas::class, 'id', 'puskesmas_id');
    }
}
