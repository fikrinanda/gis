<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Petugas extends Model
{
    use HasFactory;

    protected $table = "petugas";
    protected $fillable = ['id', 'user_id', 'puskesmas_id', 'nip', 'nama', 'foto'];
    public $incrementing = false;

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function puskesmas()
    {
        return $this->hasOne(Puskesmas::class, 'id', 'puskesmas_id');
    }
}
