<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kunjungan extends Model
{
    use HasFactory;

    protected $table = 'kunjungan';
    protected $fillable = ['puskesmas_id', 'rawat_jalan', 'rawat_inap', 'tahun', 'hash'];

    public function puskesmas()
    {
        return $this->hasOne(Puskesmas::class, 'id', 'puskesmas_id');
    }
}
