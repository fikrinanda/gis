<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Puskesmas extends Model
{
    use HasFactory;

    protected $table = 'puskesmas';
    protected $fillable = ['id', 'wilayah_id', 'nama', 'alamat', 'lat', 'lng', 'jenis', 'tahun'];
    public $incrementing = false;

    public function wilayah()
    {
        return $this->hasOne(Wilayah::class, 'id', 'wilayah_id');
    }
}
