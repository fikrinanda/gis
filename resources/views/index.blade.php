<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <h1>Halo</h1>

    @if(session('status'))
    <div>
        <h3>{{session('status')}}</h3>
    </div>
    @endif

    @if (isset($errors) && $errors->any())
    <div>
        @foreach ($errors->all() as $error)
        <h3>{{ $error }}</h3>
        @endforeach
    </div>
    @endif

    @if (session()->has('failures'))

    <table>
        <tr>
            <th>Row</th>
            <th>Attribute</th>
            <th>Errors</th>
            <th>Value</th>
        </tr>

        @foreach (session()->get('failures') as $validation)
        <tr>
            <td>{{ $validation->row() }}</td>
            <td>{{ $validation->attribute() }}</td>
            <td>
                <ul>
                    @foreach ($validation->errors() as $e)
                    <li>{{ $e }}</li>
                    @endforeach
                </ul>
            </td>
            <td>
                {{ $validation->values()[$validation->attribute()] }}
            </td>
        </tr>
        @endforeach
    </table>

    @endif

    <form action="/import/wilayah" method="post" enctype="multipart/form-data">
        @csrf
        <h3>Wilayah</h3>
        <input type="file" name="file">
        <button type="submit">Import</button>
    </form>

    <form action="/import/puskesmas" method="post" enctype="multipart/form-data">
        @csrf
        <h3>Puskesmas</h3>
        <input type="file" name="file">
        <button type="submit">Import</button>
    </form>

    <form action="/import/umum" method="post" enctype="multipart/form-data">
        @csrf
        <h3>Dokter Umum</h3>
        <input type="file" name="file">
        <button type="submit">Import</button>
    </form>

    <form action="/import/penduduk" method="post" enctype="multipart/form-data">
        @csrf
        <h3>Penduduk</h3>
        <input type="file" name="file">
        <button type="submit">Import</button>
    </form>


</body>

</html>