<div class="content-body">
    <div class="container-fluid">
        <div class="row gutters-sm">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center mb-5">
                            <img src="/storage/{{$user->petugas->foto}}" alt="Admin" width="300" height="350">
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">NIP</h6>
                            </div>
                            <div class="col-sm-9" style="color:black;">
                                {{$user->petugas->nip}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Nama</h6>
                            </div>
                            <div class="col-sm-9" style="color:black;">
                                {{$user->petugas->nama}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Username</h6>
                            </div>
                            <div class="col-sm-9" style="color:black;">
                                {{$user->username}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Puskesmas</h6>
                            </div>
                            <div class="col-sm-9" style="color:black;">
                                {{$user->petugas->puskesmas->nama}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>