<div class="content-body">
    <div class="container-fluid">
        <h1 class="text-center">Sistem Informasi Geografis</h1>
        <h1 class="text-center">Persebaran Puskesmas di Kediri Raya</h1>
        <h1 class="text-center">Tahun {{$t}}</h1>
        <br>
        <!-- Vectormap -->
        <div class="row">

            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <h4>
                                    Jumlah Puskesmas Rawat Inap
                                </h4>
                                <h4>
                                    {{App\Models\Puskesmas::where('jenis', 'Rawat Inap')->where('tahun', '<=', $t)->count()}}
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <h4>
                                    Jumlah Puskesmas Non Rawat Inap
                                </h4>
                                <h4>
                                    {{App\Models\Puskesmas::where('jenis', 'Non Rawat Inap')->where('tahun', '<=', $t)->count()}}
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <h4>
                                    Jumlah Dokter Umum
                                </h4>
                                <h4>
                                    {{App\Models\Umum::where('tahun', $t)->sum('jumlah')}}
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <h4>
                                    Jumlah Dokter Gigi
                                </h4>
                                <h4>
                                    {{App\Models\Gigi::where('tahun', $t)->sum('jumlah')}}
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <h4>
                                    Jumlah Perawat
                                </h4>
                                <h4>
                                    {{App\Models\Perawat::where('tahun', $t)->sum('jumlah')}}
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <h4>
                                    Jumlah Bidan
                                </h4>
                                <h4>
                                    {{App\Models\Bidan::where('tahun', $t)->sum('jumlah')}}
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <ul class="nav nav-pills justify-content-start mb-4">
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Jumlah Puskesmas</a>
                                    <div class="dropdown-menu">
                                        <a href="#navpills2-1" class="dropdown-item" data-toggle="tab" aria-expanded="false" style="color: black;">Standar Kemenkes</a>
                                        <a href="#navpills3-1" class="dropdown-item" data-toggle="tab" aria-expanded="false" style="color: black;">Penduduk</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Jumlah Tenaga Kesehatan</a>
                                    <div class="dropdown-menu">
                                        <a href="#navpills2-2" class="dropdown-item" data-toggle="tab" aria-expanded="false" style="color: black;">Standar Kemenkes</a>
                                        <a href="#navpills3-2" class="dropdown-item" data-toggle="tab" aria-expanded="false" style="color: black;">Rasio</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a href="#navpills2-3" class="nav-link" data-toggle="tab" aria-expanded="false">Jumlah Kunjungan</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#navpills2-4" class="nav-link" data-toggle="tab" aria-expanded="false">Informasi Poli</a>
                                </li>
                            </ul>
                            <div>
                                <label for="">Filter Tahun : </label>
                                <select wire:model="tahun" class="form-select" aria-label="Default select example">
                                    <option hidden>Pilih tahun</option>
                                    @foreach($query as $q)
                                    <option value="{{$q->tahun}}">{{$q->tahun}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="tab-content">
                            <div id="navpills2-1" class="tab-pane active">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="text-center mt-3" style="font-weight: normal;">Jumlah Puskesmas Standar Kemenkes</h4>
                                        <h5 class="text-center" style="font-weight: normal;">Tahun {{$t}}</h5>
                                        <div id="map1" style="height: 560px;">
                                            <div id="state-legend" class="legend">
                                                <h5 style="font-weight: normal;">Keterangan</h5>
                                                <div><img src="{{asset('icon/mapbox-marker-icon-20px-blue.png')}}" alt="" width="10px" height="20px" style="margin-right: 5px;">Puskesmas Rawat Inap</div>
                                                <div><img src="{{asset('icon/mapbox-marker-icon-20px-pink.png')}}" alt="" width="10px" height="20px" style="margin-right: 5px;">Puskesmas Non Rawat Inap</div>
                                                <div><span style="background-color: #FF0000"></span>Kurang dari standar</div>
                                                <div><span style="background-color: #FFFF00"></span>Standar</div>
                                                <div><span style="background-color: #008000"></span>Lebih dari standar</div>
                                            </div>
                                            <div id="anu22">
                                                <label for="">Filter jenis : &nbsp;</label>
                                                <input id="satellite-v92" type="radio" name="rtoggle2" value="semua" checked="checked">
                                                <label for="satellite-v92">Semua Jenis</label>
                                                <input id="light-v102" type="radio" name="rtoggle2" value="rawat_inap">
                                                <label for="light-v102">Rawat Inap</label>
                                                <input id="dark-v102" type="radio" name="rtoggle2" value="non_rawat_inap">
                                                <label for="dark-v102">Non Rawat Inap</label>
                                            </div>
                                        </div>

                                        <div class="border mt-3">
                                            <div class="m-2">
                                                <h3>Informasi</h3>
                                                <p style="font-size: 20px;">Berdasarkan Peraturan Menteri Kesehatan Republik Indonesia Nomor 43 Tahun 2019 Tentang Pusat Kesehatan Masyarakat Bab III Pasal 10 ayat 1, puskesmas harus didirikan pada setiap kecamatan.</p>
                                                <p style="font-size: 20px;">Dikatakan kurang dari standar, apabila kecamatan tidak memiliki puskesmas.</p>
                                                <p style="font-size: 20px;">Dikatakan standar, apabila kecamatan memiliki satu puskesmas.</p>
                                                <p style="font-size: 20px;">Dikatakan lebih dari standar, apabila kecamatan memiliki lebih dari satu puskesmas.</p>
                                            </div>
                                        </div>

                                        <div id="jumlah_puskesmas" class="mt-5" style="height: 500px;"></div>

                                    </div>
                                </div>
                            </div>
                            <div id="navpills3-1" class="tab-pane">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="text-center mt-3" style="font-weight: normal;">Jumlah Puskesmas Terhadap Jumlah Penduduk</h4>
                                        <h5 class="text-center" style="font-weight: normal;">Tahun {{$t}}</h5>
                                        <div id="map11" style="height: 560px;">
                                            <div id="state-legend" class="legend">
                                                <h5 style="font-weight: normal;">Keterangan</h5>
                                                <div><img src="{{asset('icon/mapbox-marker-icon-20px-blue.png')}}" alt="" width="10px" height="20px" style="margin-right: 5px;">Puskesmas Rawat Inap</div>
                                                <div><img src="{{asset('icon/mapbox-marker-icon-20px-pink.png')}}" alt="" width="10px" height="20px" style="margin-right: 5px;">Puskesmas Non Rawat Inap</div>
                                                <div><span style="background-color: #FF0000"></span>Kurang dari standar</div>
                                                <div><span style="background-color: #FFFF00"></span>Standar</div>
                                                <div><span style="background-color: #008000"></span>Lebih dari standar</div>
                                            </div>

                                            <!-- <div id="state-legend" class="legend2">
                                        <h6 style="font-weight: normal;">Satu puskesmas setiap tahun idealnya melayani 30.000 penduduk</h6>
                                    </div> -->
                                        </div>

                                        <div class="border mt-3">
                                            <div class="m-2">
                                                <h3>Informasi</h3>
                                                <p style="font-size: 20px;">Berdasarkan profil kesehatan Kota Kediri dan profil kesehatan Kabupaten Kediri, puskesmas diharapkan dapat melayani penduduk rata-rata 30000 penduduk per satu puskesmas.</p>
                                                <p style="font-size: 20px;">Dikatakan kurang dari standar, apabila rata-rata setiap puskesmas melayani lebih dari 31000 penduduk.</p>
                                                <p style="font-size: 20px;">Dikatakan standar, apabila rata-rata setiap puskesmas melayani antara 29000 sampai 31000 penduduk.</p>
                                                <p style="font-size: 20px;">Dikatakan lebih dari standar, apabila rata-rata setiap puskesmas melayani kurang dari 29000 penduduk.</p>
                                            </div>
                                        </div>

                                        <div id="jumlah_puskesmas2" class="mt-5" style="height: 500px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills2-2" class="tab-pane">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="text-center mt-3" style="font-weight: normal;">Jumlah Tenaga Kesehatan Standar Kemenkes</h4>
                                        <h5 class="text-center" style="font-weight: normal;">Tahun {{$t}}</h5>
                                        <div id="map2" style="height: 560px;">
                                            <div id="state-legend" class="legend">
                                                <h5 style="font-weight: normal;">Keterangan</h5>
                                                <div><img src="{{asset('icon/mapbox-marker-icon-20px-blue.png')}}" alt="" width="10px" height="20px" style="margin-right: 5px;">Puskesmas Rawat Inap</div>
                                                <div><img src="{{asset('icon/mapbox-marker-icon-20px-pink.png')}}" alt="" width="10px" height="20px" style="margin-right: 5px;">Puskesmas Non Rawat Inap</div>
                                                <div><span style="background-color: #FF0000"></span>Kurang dari standar</div>
                                                <div><span style="background-color: #FFFF00"></span>Standar</div>
                                                <div><span style="background-color: #008000"></span>Lebih dari standar</div>
                                                <!-- <div><span style="background-color: #FF0000"></span>Salah satu tenaga kesehatan kurang dari standar</div>
                                        <div><span style="background-color: #FFFF00"></span>Setiap tenaga kesehatan sama dengan standar</div>
                                        <div><span style="background-color: #008000"></span>Setiap tenaga kesehatan lebih dari standar</div> -->
                                            </div>

                                            <!-- <div id="state-legend" class="legend2">
                                        <h6 style="font-weight: normal;">Standar Ketenagaan Puskesmas</h6>
                                        <div>Dokter Umum 1</div>
                                        <div>Dokter Gigi 1</div>
                                        <div>Perawat 5</div>
                                        <div>Bidan 4</div>
                                    </div> -->
                                        </div>

                                        <div class="border mt-3">
                                            <div class="m-2">
                                                <h3>Informasi</h3>
                                                <p style="font-size: 20px;">Berdasarkan Peraturan Menteri Kesehatan Republik Indonesia Nomor 43 Tahun 2019 Tentang Pusat Kesehatan Masyarakat, standar ketenagaan puskesmas : satu dokter umum, satu dokter gigi, lima perawat, dan empat bidan.</p>
                                                <p style="font-size: 20px;">Dikatakan kurang dari standar, apabila salah satu tenaga kesehatan kurang dari standar.</p>
                                                <p style="font-size: 20px;">Dikatakan standar, apabila setiap tenaga kesehatan sama dengan standar.</p>
                                                <p style="font-size: 20px;">Dikatakan lebih dari standar, apabila setiap tenaga kesehatan lebih dari standar.</p>
                                            </div>
                                        </div>

                                        <div id="tenaga_kesehatan" class="mt-5" style="height: 500px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills3-2" class="tab-pane">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="text-center mt-3" style="font-weight: normal;">Jumlah Tenaga Kesehatan Terhadap Jumlah Penduduk</h4>
                                        <h5 class="text-center" style="font-weight: normal;">Tahun {{$t}}</h5>
                                        <div id="map22" style="height: 560px;">
                                            <div id="state-legend" class="legend">
                                                <h5 style="font-weight: normal;">Keterangan</h5>
                                                <div><img src="{{asset('icon/mapbox-marker-icon-20px-blue.png')}}" alt="" width="10px" height="20px" style="margin-right: 5px;">Puskesmas Rawat Inap</div>
                                                <div><img src="{{asset('icon/mapbox-marker-icon-20px-pink.png')}}" alt="" width="10px" height="20px" style="margin-right: 5px;">Puskesmas Non Rawat Inap</div>
                                                <div><span style="background-color: #FF0000"></span>Kurang dari standar</div>
                                                <div><span style="background-color: #FFFF00"></span>Standar</div>
                                                <div><span style="background-color: #008000"></span>Lebih dari standar</div>
                                            </div>
                                            <div id="anu">
                                                <input id="satellite-v9" type="radio" name="rtoggle" value="dokter_umum" checked="checked">
                                                <label for="satellite-v9">Dokter Umum</label>
                                                <input id="light-v10" type="radio" name="rtoggle" value="dokter_gigi">
                                                <label for="light-v10">Dokter Gigi</label>
                                                <input id="dark-v10" type="radio" name="rtoggle" value="perawat">
                                                <label for="dark-v10">Perawat</label>
                                                <input id="streets-v11" type="radio" name="rtoggle" value="bidan">
                                                <label for="streets-v11">Bidan</label>
                                            </div>
                                        </div>

                                        <div class="border mt-3">
                                            <div class="m-2">
                                                <h3>Informasi</h3>
                                                <p style="font-size: 20px;">Berdasarkan Peraturan Menteri Hukum dan HAM Nomor 34 tahun 2016 terdapat rasio rasio yang digunakan sebagai berikut :</p>
                                                <p style="font-size: 20px;">1. Rasio tenaga Dokter per penduduk adalah 1 : 2500.</p>
                                                <p style="font-size: 20px;">2. Rasio tenaga Dokter Gigi per penduduk adalah 1 : 2000.</p>
                                                <p style="font-size: 20px;">3. Rasio Perawat terhadap penduduk adalah 1 : 855.</p>
                                                <p style="font-size: 20px;">4. Rasio Bidan terhadap penduduk adalah 1 : 1.000.</p>
                                            </div>
                                        </div>

                                        <div id="tenaga_kesehatan2" class="mt-5" style="height: 500px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills2-3" class="tab-pane">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="text-center mt-3" style="font-weight: normal;">Jumlah Kunjungan</h4>
                                        <h5 class="text-center" style="font-weight: normal;">Tahun {{$t}}</h5>
                                        <div id="map3" style="height: 560px;">
                                            <div id="state-legend" class="legend">
                                                <h5 style="font-weight: normal;">Keterangan</h5>
                                                <div><img src="{{asset('icon/mapbox-marker-icon-20px-blue.png')}}" alt="" width="10px" height="20px" style="margin-right: 5px;">Puskesmas Rawat Inap</div>
                                                <div><img src="{{asset('icon/mapbox-marker-icon-20px-pink.png')}}" alt="" width="10px" height="20px" style="margin-right: 5px;">Puskesmas Non Rawat Inap</div>
                                            </div>
                                        </div>

                                        <div id="kunjungan" class="mt-5" style="height: 500px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div id="navpills2-4" class="tab-pane">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="text-center mt-3" style="font-weight: normal;">Informasi Poli {{$nama}}</h4>
                                        <h5 class="text-center" style="font-weight: normal;">Tahun {{$t}}</h5>

                                        <div id="map4" style="height: 560px;">
                                            <div id="state-legend" class="legend">
                                                <h5 style="font-weight: normal;">Keterangan</h5>
                                                <div><img src="{{asset('icon/mapbox-marker-icon-20px-blue.png')}}" alt="" width="10px" height="20px" style="margin-right: 5px;">Puskesmas Rawat Inap</div>
                                                <div><img src="{{asset('icon/mapbox-marker-icon-20px-pink.png')}}" alt="" width="10px" height="20px" style="margin-right: 5px;">Puskesmas Non Rawat Inap</div>
                                            </div>
                                            <div id="anu">
                                                <label for="">Filter poli : &nbsp;</label>
                                                <select wire:model="pl" class="form-select" aria-label="Default select example">
                                                    <option hidden>Pilih poli</option>
                                                    <option value="">Semua</option>
                                                    @foreach($poli as $q)
                                                    <option value="{{$q->poli}}">{{$q->poli}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <!-- <div id="poli" class="mt-5" style="height: 500px;"></div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    var device;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        // true for mobile device
        device = [
            [111.75913914489564, -8.669228758626588],
            [112.45263414965126, -7.170129037865038]
        ];
    } else {
        // false for not mobile device
        device = [
            [111.52119039287481, -8.040812976926816],
            [112.69129014605602, -7.586629911014967]
        ];
    }

    mapboxgl.accessToken = 'pk.eyJ1IjoiZmlrcmluYW5kYSIsImEiOiJja2RsbmY3djgxMGlxMnlwZDBha213b3hpIn0.wR7buPys4UOuFLbJJqHzIA';
    // Set bounds to New York, New York

    var map1 = new mapboxgl.Map({
        container: 'map1',
        style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        center: [112.11224465310153, -7.841377756255071], // starting position [lng, lat]
        zoom: 8.3,
        maxBounds: device,
    });
    var hoveredStateId1 = null;

    // map1.on('style.load', function() {
    //     map1.on('click', function(e) {
    //         var coordinates = e.lngLat;
    //         new mapboxgl.Popup()
    //             .setLngLat(coordinates)
    //             .setHTML('you clicked here: <br/>' + coordinates)
    //             .addTo(map1);
    //     });
    // });

    map1.addControl(new mapboxgl.NavigationControl());
    map1.addControl(new mapboxgl.FullscreenControl());

    map1.on('load', function() {
        map1.loadImage(
            <?php echo  "'" . asset('icon/mapbox-marker-icon-20px-blue.png') . "'"; ?>,
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map1.addImage('blue', image);
            }
        );

        map1.loadImage(
            <?php echo "'" . asset('icon/mapbox-marker-icon-20px-pink.png') . "'"; ?>,
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map1.addImage('pink', image);
            }
        );

        map1.addSource('maine', {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': [
                    <?php

                    use App\Models\{Puskesmas, Penduduk};

                    $a = '{';
                    $b = ',';

                    foreach ($wilayah as $wly) {
                        $id = substr($wly->id, -4);
                        $rawat_inap = Puskesmas::where('wilayah_id', $wly->id)->where('jenis', 'Rawat Inap')->where('tahun', '<=', $t)->count();
                        $non_rawat_inap = Puskesmas::where('wilayah_id', $wly->id)->where('jenis', 'Non Rawat Inap')->where('tahun', '<=', $t)->count();
                        $jumlah_puskesmas = $rawat_inap + $non_rawat_inap;
                        echo $a;
                        echo file_get_contents('storage/' . $wly->file); // get the contents, and echo it out.
                        echo $b;
                        echo '"id":' . '"' . $id . '",';
                        echo '"properties": {"nama": ' . '"' . $wly->nama . '"' . ',"wilayah": "Kota Kediri","rawat_inap":' . $rawat_inap . ',"non_rawat_inap":' . $non_rawat_inap . ',"jumlah_puskesmas":' . $jumlah_puskesmas . '}},';
                    }
                    ?>
                ]
            }
        });

        map1.addLayer({
            'id': 'isi',
            'type': 'fill',
            'source': 'maine',
            'layout': {},
            'paint': {
                'fill-color': [
                    "case",
                    ['<=', ['get', "jumlah_puskesmas"], 0], "#FF0000",
                    ['<=', ['get', "jumlah_puskesmas"], 1], "#FFFF00",
                    ['<=', ['get', "jumlah_puskesmas"], 2], "#008000",
                    ['>', ['get', "jumlah_puskesmas"], 2], "#008000",
                    '#123456'
                ],
                'fill-opacity': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    1,
                    0.5
                ]
            }
        });

        map1.addLayer({
            'id': 'batas',
            'type': 'line',
            'source': 'maine',
            'layout': {},
            'paint': {
                'line-width': 1.5,
                'line-color': '#F0FFFF'
            }
        });

        map1.addSource('places', {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': [
                    <?php
                    foreach ($puskesmas as $pk) {
                        echo "{
                                    'type': 'Feature',
                                    'properties': {
                                        'nama': '$pk->nama',
                                        'alamat': '$pk->alamat',
                                        'jenis': '$pk->jenis',
                                        'icon': 'theatre'
                                    },
                                    'geometry': {
                                        'type': 'Point',
                                        'coordinates': [
                                            ";
                        echo $pk->lng . ',' . $pk->lat;
                        echo "
                                        ]
                                    }
                                }";
                        echo ',';
                    }
                    ?>
                ]
            }
        });
        // Add a layer showing the places.
        map1.addLayer({
            'id': 'places',
            'type': 'symbol',
            'source': 'places',
            'layout': {
                'icon-image': [
                    "case",
                    [">=",
                        ["to-string", ["get", "jenis"]],
                        ["to-string", "Rawat Inap"]
                    ], "blue", "pink"
                ],
                'icon-allow-overlap': true
            }
        });

        var layerList2 = document.getElementById('anu22');
        var inputs2 = layerList2.getElementsByTagName('input');
        let abc2 = "semua";

        function switchLayer2(layer) {
            var layerId2 = layer.target.value;
            abc2 = layerId2;
            if (abc2 == 'semua') {
                map1.setLayoutProperty('places', 'icon-image', ["case",
                    [">=",
                        ["to-string", ["get", "jenis"]],
                        ["to-string", "Rawat Inap"]
                    ], "blue", "pink"
                ]);
            } else if (abc2 == 'rawat_inap') {
                map1.setLayoutProperty('places', 'icon-image', ["case",
                    [">=",
                        ["to-string", ["get", "jenis"]],
                        ["to-string", "Rawat Inap"]
                    ], "blue", ""
                ]);
            } else if (abc2 == 'non_rawat_inap') {
                map1.setLayoutProperty('places', 'icon-image', ["case",
                    [">=",
                        ["to-string", ["get", "jenis"]],
                        ["to-string", "Rawat Inap"]
                    ], "", "pink"
                ]);
            }
        }

        for (var i = 0; i < inputs2.length; i++) {
            inputs2[i].onclick = switchLayer2;
        }

        var popup1 = new mapboxgl.Popup({
            closeButton: false,
            closeOnClick: false
        });

        map1.on('mousemove', 'isi', function(e) {
            if (e.features.length > 0) {
                if (hoveredStateId1) {
                    // Change the cursor style as a UI indicator.
                    map1.getCanvas().style.cursor = 'pointer';

                    // Single out the first found feature.
                    var feature1 = e.features[0];
                    if ((feature1.properties.rawat_inap != 0) && (feature1.properties.non_rawat_inap != 0)) {
                        var hehe = '<h6>' + "Kecamatan " + feature1.properties.nama + '</h6>' + "Tersedia " + feature1.properties.non_rawat_inap + " puskesmas non rawat inap" + '<br>' + "Tersedia " + feature1.properties.rawat_inap + " puskesmas rawat inap";
                    } else if ((feature1.properties.rawat_inap == 0) && (feature1.properties.non_rawat_inap == 0)) {
                        var hehe = '<h6>' + "Kecamatan " + feature1.properties.nama + '</h6>' + "Tidak tersedia puskesmas non rawat inap" + '<br>' + "Tidak tersedia puskesmas rawat inap";
                    } else if (feature1.properties.non_rawat_inap == 0) {
                        var hehe = '<h6>' + "Kecamatan " + feature1.properties.nama + '</h6>' + "Tidak tersedia puskesmas non rawat inap" + '<br>' + "Tersedia " + feature1.properties.rawat_inap + " puskesmas rawat inap";
                    } else if (feature1.properties.rawat_inap == 0) {
                        var hehe = '<h6>' + "Kecamatan " + feature1.properties.nama + '</h6>' + "Tersedia " + feature1.properties.non_rawat_inap + " puskesmas non rawat inap" + '<br>' + "Tidak tersedia puskesmas rawat inap";
                    }

                    // Display a popup with the name of the county
                    popup1.setLngLat(e.lngLat)
                        .setHTML(hehe)
                        .addTo(map1);
                    map1.setFeatureState({
                        source: 'maine',
                        id: hoveredStateId1
                    }, {
                        hover: false
                    });
                }
                hoveredStateId1 = e.features[0].id;
                map1.setFeatureState({
                    source: 'maine',
                    id: hoveredStateId1
                }, {
                    hover: true
                });
            }
        });

        map1.on('mouseleave', 'isi', function() {
            if (hoveredStateId1) {
                map1.getCanvas().style.cursor = '';
                popup1.remove();
                map1.setFeatureState({
                    source: 'maine',
                    id: hoveredStateId1
                }, {
                    hover: false
                });
            }
            hoveredStateId1 = null;
        });

        map1.on('idle', function() {
            map1.resize()
        });

        // When a click event occurs on a feature in the places layer, open a popup at the
        // location of the feature, with description HTML from its properties.
        map1.on('click', 'places', function(e) {
            var coordinates1 = e.features[0].geometry.coordinates.slice();
            var nama1 = e.features[0].properties.nama;
            var alamat1 = e.features[0].properties.alamat;
            var jenis1 = e.features[0].properties.jenis;

            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(e.lngLat.lng - coordinates1[0]) > 180) {
                coordinates1[0] += e.lngLat.lng > coordinates1[0] ? 360 : -360;
            }

            new mapboxgl.Popup()
                .setLngLat(coordinates1)
                .setHTML('<h6>' + "Puskesmas " + nama1 + '</h6>' + alamat1 + '<br>' + jenis1)
                .addTo(map1);
        });

        // Change the cursor to a pointer when the mouse is over the places layer.
        map1.on('mouseenter', 'places', function() {
            map1.getCanvas().style.cursor = 'pointer';
        });

        // Change it back to a pointer when it leaves.
        map1.on('mouseleave', 'places', function() {
            map1.getCanvas().style.cursor = '';
        });

    });
</script>

<script>
    mapboxgl.accessToken = 'pk.eyJ1IjoiZmlrcmluYW5kYSIsImEiOiJja2RsbmY3djgxMGlxMnlwZDBha213b3hpIn0.wR7buPys4UOuFLbJJqHzIA';
    // Set bounds to New York, New York

    var map11 = new mapboxgl.Map({
        container: 'map11',
        style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        center: [112.11224465310153, -7.841377756255071], // starting position [lng, lat]
        zoom: 8.3,
        maxBounds: device,
    });
    var hoveredStateId11 = null;

    // map11.on('style.load', function() {
    //     map11.on('click', function(e) {
    //         var coordinates = e.lngLat;
    //         new mapboxgl.Popup()
    //             .setLngLat(coordinates)
    //             .setHTML('you clicked here: <br/>' + coordinates)
    //             .addTo(map11);
    //     });
    // });

    map11.addControl(new mapboxgl.NavigationControl());
    map11.addControl(new mapboxgl.FullscreenControl());

    map11.on('load', function() {
        map11.loadImage(
            <?php echo  "'" . asset('icon/mapbox-marker-icon-20px-blue.png') . "'"; ?>,
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map11.addImage('blue', image);
            }
        );

        map11.loadImage(
            <?php echo "'" . asset('icon/mapbox-marker-icon-20px-pink.png') . "'"; ?>,
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map11.addImage('pink', image);
            }
        );

        map11.addSource('maine', {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': [
                    <?php

                    $a = '{';
                    $b = ',';

                    foreach ($wilayah as $wly) {
                        $id = substr($wly->id, -4);
                        $rawat_inap = Puskesmas::where('wilayah_id', $wly->id)->where('jenis', 'Rawat Inap')->where('tahun', '<=', $t)->count();
                        $non_rawat_inap = Puskesmas::where('wilayah_id', $wly->id)->where('jenis', 'Non Rawat Inap')->where('tahun', '<=', $t)->count();
                        $p = Puskesmas::where('wilayah_id', $wly->id)->where('tahun', '<=', $t)->count();
                        $jumlah_puskesmas = $rawat_inap + $non_rawat_inap;
                        $penduduk = Penduduk::where('wilayah_id', $wly->id)->where('tahun', $t)->first();
                        if (isset($penduduk->jumlah)) {
                            $bagi = $penduduk->jumlah / $p;
                            $jumlah = $penduduk->jumlah;
                        } else {
                            $bagi = 0;
                            $jumlah = 0;
                        }
                        echo $a;
                        echo file_get_contents('storage/' . $wly->file); // get the contents, and echo it out.
                        echo $b;
                        echo '"id":' . '"' . $id . '",';
                        echo '"properties": {"nama": ' . '"' . $wly->nama . '"' . ',"wilayah": "Kota Kediri","rawat_inap":' . $rawat_inap . ',"non_rawat_inap":' . $non_rawat_inap . ',"jumlah_puskesmas":' . $jumlah_puskesmas . ',"bagi":' . round($bagi) . ',"penduduk":' . $jumlah . '}},';
                    }
                    ?>
                ]
            }
        });

        map11.addLayer({
            'id': 'isi',
            'type': 'fill',
            'source': 'maine',
            'layout': {},
            'paint': {
                'fill-color': [
                    "case",
                    ['<', ['get', "bagi"], 29000], "#008000",
                    ['>', ['get', "bagi"], 31000], "#FF0000",
                    ['>=', ['get', "bagi"], 29000], "#FFFF00",
                    ['<=', ['get', "bagi"], 31000], "#FFFF00",
                    '#123456'
                ],
                'fill-opacity': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    1,
                    0.5
                ]
            }
        });

        map11.addLayer({
            'id': 'batas',
            'type': 'line',
            'source': 'maine',
            'layout': {},
            'paint': {
                'line-width': 1.5,
                'line-color': '#F0FFFF'
            }
        });

        map11.addSource('places', {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': [
                    <?php
                    foreach ($puskesmas as $pk) {
                        echo "{
                                    'type': 'Feature',
                                    'properties': {
                                        'nama': '$pk->nama',
                                        'alamat': '$pk->alamat',
                                        'jenis': '$pk->jenis',
                                        'icon': 'theatre'
                                    },
                                    'geometry': {
                                        'type': 'Point',
                                        'coordinates': [
                                            ";
                        echo $pk->lng . ',' . $pk->lat;
                        echo "
                                        ]
                                    }
                                }";
                        echo ',';
                    }
                    ?>
                ]
            }
        });
        // Add a layer showing the places.
        map11.addLayer({
            'id': 'places',
            'type': 'symbol',
            'source': 'places',
            'layout': {
                'icon-image': [
                    "case",
                    [">=",
                        ["to-string", ["get", "jenis"]],
                        ["to-string", "Rawat Inap"]
                    ], "blue", "pink"
                ],
                'icon-allow-overlap': true
            }
        });

        var popup11 = new mapboxgl.Popup({
            closeButton: false,
            closeOnClick: false
        });

        map11.on('mousemove', 'isi', function(e) {
            if (e.features.length > 0) {
                if (hoveredStateId11) {
                    // Change the cursor style as a UI indicator.
                    map11.getCanvas().style.cursor = 'pointer';

                    // Single out the first found feature.
                    var feature11 = e.features[0];

                    var hehe2 = '<h6>' + "Kecamatan " + feature11.properties.nama + '</h6>' + "Total tersedia " + feature11.properties.jumlah_puskesmas + " puskesmas" + '<br>' + "Jumlah penduduk " + feature11.properties.penduduk + '<br>' + "Rata-rata setiap puskesmas melayani " + feature11.properties.bagi + " penduduk";

                    // Display a popup with the name of the county
                    popup11.setLngLat(e.lngLat)
                        .setHTML(hehe2)
                        .addTo(map11);
                    map11.setFeatureState({
                        source: 'maine',
                        id: hoveredStateId11
                    }, {
                        hover: false
                    });
                }
                hoveredStateId11 = e.features[0].id;
                map11.setFeatureState({
                    source: 'maine',
                    id: hoveredStateId11
                }, {
                    hover: true
                });
            }
        });

        map11.on('mouseleave', 'isi', function() {
            if (hoveredStateId11) {
                map11.getCanvas().style.cursor = '';
                popup11.remove();
                map11.setFeatureState({
                    source: 'maine',
                    id: hoveredStateId11
                }, {
                    hover: false
                });
            }
            hoveredStateId11 = null;
        });

        map11.on('idle', function() {
            map11.resize()
        });

        // When a click event occurs on a feature in the places layer, open a popup at the
        // location of the feature, with description HTML from its properties.
        map11.on('click', 'places', function(e) {
            var coordinates11 = e.features[0].geometry.coordinates.slice();
            var nama11 = e.features[0].properties.nama;
            var alamat11 = e.features[0].properties.alamat;
            var jenis11 = e.features[0].properties.jenis;

            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(e.lngLat.lng - coordinates11[0]) > 180) {
                coordinates11[0] += e.lngLat.lng > coordinates11[0] ? 360 : -360;
            }

            new mapboxgl.Popup()
                .setLngLat(coordinates11)
                .setHTML('<h6>' + "Puskesmas " + nama11 + '</h6>' + alamat11 + '<br>' + jenis11)
                .addTo(map11);
        });

        // Change the cursor to a pointer when the mouse is over the places layer.
        map11.on('mouseenter', 'places', function() {
            map11.getCanvas().style.cursor = 'pointer';
        });

        // Change it back to a pointer when it leaves.
        map11.on('mouseleave', 'places', function() {
            map11.getCanvas().style.cursor = '';
        });

    });
</script>

<script>
    mapboxgl.accessToken = 'pk.eyJ1IjoiZmlrcmluYW5kYSIsImEiOiJja2RsbmY3djgxMGlxMnlwZDBha213b3hpIn0.wR7buPys4UOuFLbJJqHzIA';
    // Set bounds to New York, New York

    var map2 = new mapboxgl.Map({
        container: 'map2',
        style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        center: [112.11224465310153, -7.841377756255071], // starting position [lng, lat]
        zoom: 8.3,
        maxBounds: device,
    });
    var hoveredStateId2 = null;

    map2.addControl(new mapboxgl.NavigationControl());
    map2.addControl(new mapboxgl.FullscreenControl());

    map2.on('load', function() {
        map2.loadImage(
            <?php echo  "'" . asset('icon/mapbox-marker-icon-20px-blue.png') . "'"; ?>,
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map2.addImage('blue', image);
            }
        );

        map2.loadImage(
            <?php echo "'" . asset('icon/mapbox-marker-icon-20px-pink.png') . "'"; ?>,
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map2.addImage('pink', image);
            }
        );

        map2.addSource('maine', {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': [
                    <?php

                    use App\Models\Umum;
                    use App\Models\Gigi;
                    use App\Models\Perawat;
                    use App\Models\Bidan;

                    $a = '{';
                    $b = ',';
                    $dokter_umum = 0;
                    $dokter_gigi = 0;
                    $perawat = 0;
                    $bidan = 0;

                    foreach ($wilayah as $wly) {
                        $id = substr($wly->id, -4);
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        foreach ($pk as $key2) {
                            $d = Umum::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($d->jumlah)) {
                                $dokter_umum += $d->jumlah;
                            }
                            $g = Gigi::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($g->jumlah)) {
                                $dokter_gigi += $g->jumlah;
                            }
                            $pt = Perawat::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($pt->jumlah)) {
                                $perawat += $pt->jumlah;
                            }
                            $bn = Bidan::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($bn->jumlah)) {
                                $bidan += $bn->jumlah;
                            }
                        }
                        echo $a;
                        echo file_get_contents('storage/' . $wly->file); // get the contents, and echo it out.
                        echo $b;
                        echo '"id":' . '"' . $id . '",';
                        echo '"properties": {"nama": ' . '"' . $wly->nama . '"' . ',"wilayah": "Kota Kediri","dokter_umum":' . $dokter_umum . ',"dokter_gigi":' . $dokter_gigi . ',"perawat":' . $perawat . ',"bidan":' . $bidan . '}},';
                        $dokter_umum = 0;
                        $dokter_gigi = 0;
                        $perawat = 0;
                        $bidan = 0;
                    }
                    ?>
                ]
            }
        });
        map2.addLayer({
            'id': 'isi',
            'type': 'fill',
            'source': 'maine',
            'layout': {},
            'paint': {
                'fill-color': [
                    "case",

                    ['<', ['get', 'dokter_umum'], 1], "#FF0000",
                    ['<', ['get', 'dokter_gigi'], 1], "#FF0000",
                    ['<', ['get', 'perawat'], 5], "#FF0000",
                    ['<', ['get', 'bidan'], 4], "#FF0000",

                    ['==', ['get', 'dokter_umum'], 1], "#FFFF00",
                    ['==', ['get', 'dokter_gigi'], 1], "#FFFF00",
                    ['==', ['get', 'perawat'], 5], "#FFFF00",
                    ['==', ['get', 'bidan'], 4], "#FFFF00",

                    ['>', ['get', 'dokter_umum'], 1], "#008000",
                    ['>', ['get', 'dokter_gigi'], 1], "#008000",
                    ['>', ['get', 'perawat'], 5], "#008000",
                    ['>', ['get', 'bidan'], 4], "#008000",



                    // ['all',
                    //     ['>', ['get', 'dokter_umum'], 1],
                    //     ['==', ['get', 'dokter_gigi'], 1],
                    //     ['==', ['get', 'perawat'], 5],
                    //     ['==', ['get', 'bidan'], 4],
                    // ],
                    // "#FFFF00",

                    // ['all',
                    //     ['==', ['get', 'dokter_umum'], 1],
                    //     ['>', ['get', 'dokter_gigi'], 1],
                    //     ['==', ['get', 'perawat'], 5],
                    //     ['==', ['get', 'bidan'], 4],
                    // ],
                    // "#FFFF00",

                    // ['all',
                    //     ['==', ['get', 'dokter_umum'], 1],
                    //     ['==', ['get', 'dokter_gigi'], 1],
                    //     ['>', ['get', 'perawat'], 5],
                    //     ['==', ['get', 'bidan'], 4],
                    // ],
                    // "#FFFF00",

                    // ['all',
                    //     ['==', ['get', 'dokter_umum'], 1],
                    //     ['==', ['get', 'dokter_gigi'], 1],
                    //     ['==', ['get', 'perawat'], 5],
                    //     ['>', ['get', 'bidan'], 4],
                    // ],
                    // "#FFFF00",

                    // "#FF0000",
                    // ['<=', ['get', "dokter_umum"], 0], "#FF0000",
                    // ['<=', ['get', "dokter_umum"], 1], "#FFFF00",
                    // ['<=', ['get', "dokter_umum"], 2], "#008000",
                    // ['>', ['get', "dokter_umum"], 2], "#008000",
                    '#123456'
                ],
                'fill-opacity': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    1,
                    0.5
                ]
            }
        });
        map2.addLayer({
            'id': 'batas',
            'type': 'line',
            'source': 'maine',
            'layout': {},
            'paint': {
                'line-width': 1.5,
                'line-color': '#F0FFFF'
            }
        });

        map2.addSource('places', {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': [
                    <?php
                    foreach ($puskesmas as $pk) {
                        $du = Umum::where('puskesmas_id', $pk->id)->where('tahun', $t)->first();
                        $gi = Gigi::where('puskesmas_id', $pk->id)->where('tahun', $t)->first();
                        $pe = Perawat::where('puskesmas_id', $pk->id)->where('tahun', $t)->first();
                        $bi = Bidan::where('puskesmas_id', $pk->id)->where('tahun', $t)->first();
                        echo "{
                                    'type': 'Feature',
                                    'properties': {
                                        'nama': '$pk->nama',
                                        'alamat': '$pk->alamat',
                                        'jenis': '$pk->jenis',
                                        'dokter_umum': ";
                        if (isset($du->jumlah)) {
                            echo "'" . $du->jumlah . "'";
                        } else {
                            echo '0';
                        }
                        echo ",
                                        'dokter_gigi': ";
                        if (isset($gi->jumlah)) {
                            echo "'" . $gi->jumlah . "'";
                        } else {
                            echo '0';
                        }
                        echo ",
                                        'perawat': ";
                        if (isset($pe->jumlah)) {
                            echo "'" . $pe->jumlah . "'";
                        } else {
                            echo '0';
                        }
                        echo ",
                                        'bidan': ";
                        if (isset($bi->jumlah)) {
                            echo "'" . $bi->jumlah . "'";
                        } else {
                            echo '0';
                        }
                        echo ",
                                        'icon': 'theatre'
                                    },
                                    'geometry': {
                                        'type': 'Point',
                                        'coordinates': [
                                            ";
                        echo $pk->lng . ',' . $pk->lat;
                        echo "
                                        ]
                                    }
                                }";
                        echo ',';
                    }
                    ?>
                ]
            }
        });
        // Add a layer showing the places.
        map2.addLayer({
            'id': 'places',
            'type': 'symbol',
            'source': 'places',
            'layout': {
                'icon-image': [
                    "case",
                    [">=",
                        ["to-string", ["get", "jenis"]],
                        ["to-string", "Rawat Inap"]
                    ], "blue", "pink"
                ],
                'icon-allow-overlap': true
            }
        });

        var popup2 = new mapboxgl.Popup({
            closeButton: false,
            closeOnClick: false
        });

        map2.on('mousemove', 'isi', function(e) {
            if (e.features.length > 0) {
                if (hoveredStateId2) {
                    // Change the cursor style as a UI indicator.
                    map2.getCanvas().style.cursor = 'pointer';

                    // Single out the first found feature.
                    var feature2 = e.features[0];

                    // Display a popup with the name of the county
                    popup2.setLngLat(e.lngLat)
                        .setHTML('<h6>' + "Kecamatan " + feature2.properties.nama + '</h6>' + "Memiliki " + feature2.properties.dokter_umum + " dokter umum" + '<br>' + "Memiliki " + feature2.properties.dokter_gigi + " dokter gigi" + '<br>' + "Memiliki " + feature2.properties.perawat + "  perawat" + '<br>' + "Memiliki " + feature2.properties.bidan + " bidan")
                        .addTo(map2);
                    map2.setFeatureState({
                        source: 'maine',
                        id: hoveredStateId2
                    }, {
                        hover: false
                    });
                }
                hoveredStateId2 = e.features[0].id;
                map2.setFeatureState({
                    source: 'maine',
                    id: hoveredStateId2
                }, {
                    hover: true
                });
            }
        });

        map2.on('mouseleave', 'isi', function() {
            if (hoveredStateId2) {
                map2.getCanvas().style.cursor = '';
                popup2.remove();
                map2.setFeatureState({
                    source: 'maine',
                    id: hoveredStateId2
                }, {
                    hover: false
                });
            }
            hoveredStateId2 = null;
        });

        map2.on('idle', function() {
            map2.resize()
        });

        // When a click event occurs on a feature in the places layer, open a popup at the
        // location of the feature, with description HTML from its properties.
        map2.on('click', 'places', function(e) {
            var coordinates2 = e.features[0].geometry.coordinates.slice();
            var nama2 = e.features[0].properties.nama;
            var alamat2 = e.features[0].properties.alamat;
            var jenis2 = e.features[0].properties.jenis;
            var dokter_umum2 = e.features[0].properties.dokter_umum;
            var dokter_gigi2 = e.features[0].properties.dokter_gigi;
            var perawat2 = e.features[0].properties.perawat;
            var bidan2 = e.features[0].properties.bidan;

            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(e.lngLat.lng - coordinates2[0]) > 180) {
                coordinates2[0] += e.lngLat.lng > coordinates2[0] ? 360 : -360;
            }

            new mapboxgl.Popup()
                .setLngLat(coordinates2)
                .setHTML('<h6>' + "Puskesmas " + nama2 + '</h6>' + alamat2 + '<br>' + jenis2 + '<br>' + "Memiliki " + dokter_umum2 + " dokter umum" + '<br>' + "Memiliki " + dokter_gigi2 + " dokter gigi" + '<br>' + "Memiliki " + perawat2 + " perawat" + '<br>' + "Memiliki " + bidan2 + " bidan")
                .addTo(map2);
        });

        // Change the cursor to a pointer when the mouse is over the places layer.
        map2.on('mouseenter', 'places', function() {
            map2.getCanvas().style.cursor = 'pointer';
        });

        // Change it back to a pointer when it leaves.
        map2.on('mouseleave', 'places', function() {
            map2.getCanvas().style.cursor = '';
        });

    });
</script>

<script>
    mapboxgl.accessToken = 'pk.eyJ1IjoiZmlrcmluYW5kYSIsImEiOiJja2RsbmY3djgxMGlxMnlwZDBha213b3hpIn0.wR7buPys4UOuFLbJJqHzIA';
    // Set bounds to New York, New York

    var map22 = new mapboxgl.Map({
        container: 'map22',
        style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        center: [112.11224465310153, -7.841377756255071], // starting position [lng, lat]
        zoom: 8.3,
        maxBounds: device,
    });
    var hoveredStateId22 = null;

    map22.addControl(new mapboxgl.NavigationControl());
    map22.addControl(new mapboxgl.FullscreenControl());

    map22.on('load', function() {
        map22.loadImage(
            <?php echo  "'" . asset('icon/mapbox-marker-icon-20px-blue.png') . "'"; ?>,
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map22.addImage('blue', image);
            }
        );

        map22.loadImage(
            <?php echo "'" . asset('icon/mapbox-marker-icon-20px-pink.png') . "'"; ?>,
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map22.addImage('pink', image);
            }
        );

        map22.addSource('maine', {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': [
                    <?php

                    $a = '{';
                    $b = ',';
                    $dokter_umum = 0;
                    $dokter_gigi = 0;
                    $perawat = 0;
                    $bidan = 0;

                    foreach ($wilayah as $wly) {
                        $id = substr($wly->id, -4);
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        $tt = Penduduk::where('wilayah_id', $wly->id)->where('tahun', $t);
                        if ($tt->exists()) {
                            $pdd = $tt->first()->jumlah;
                        } else {
                            $pdd = 0;
                        }
                        foreach ($pk as $key2) {
                            $d = Umum::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($d->jumlah)) {
                                $dokter_umum += $d->jumlah;
                            }
                            $g = Gigi::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($g->jumlah)) {
                                $dokter_gigi += $g->jumlah;
                            }
                            $pt = Perawat::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($pt->jumlah)) {
                                $perawat += $pt->jumlah;
                            }
                            $bn = Bidan::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($bn->jumlah)) {
                                $bidan += $bn->jumlah;
                            }
                        }
                        if ($dokter_umum > 0) {
                            $dokter_umum_bagi = round($pdd / $dokter_umum);
                        } else {
                            $dokter_umum_bagi = 0;
                        }
                        if ($dokter_gigi > 0) {
                            $dokter_gigi_bagi = round($pdd / $dokter_gigi);
                        } else {
                            $dokter_gigi_bagi = 0;
                        }
                        if ($perawat > 0) {
                            $perawat_bagi = round($pdd / $perawat);
                        } else {
                            $perawat_bagi = 0;
                        }
                        if ($bidan > 0) {
                            $bidan_bagi = round($pdd / $bidan);
                        } else {
                            $bidan_bagi = 0;
                        }

                        echo $a;
                        echo file_get_contents('storage/' . $wly->file); // get the contents, and echo it out.
                        echo $b;
                        echo '"id":' . '"' . $id . '",';
                        echo '"properties": {"nama": ' . '"' . $wly->nama . '"' . ',"wilayah": "Kota Kediri","dokter_umum":' . $dokter_umum . ',"dokter_gigi":' . $dokter_gigi . ',"perawat":' . $perawat . ',"bidan":' . $bidan . ',"penduduk":' . $pdd . ',"dokter_umum_bagi":' . $dokter_umum_bagi . ',"dokter_gigi_bagi":' . $dokter_gigi_bagi . ',"perawat_bagi":' . $perawat_bagi . ',"bidan_bagi":' . $bidan_bagi . '}},';
                        $dokter_umum = 0;
                        $dokter_gigi = 0;
                        $perawat = 0;
                        $bidan = 0;
                        $dokter_umum_bagi = 0;
                        $dokter_gigi_bagi = 0;
                        $perawat_bagi = 0;
                        $bidan_bagi = 0;
                    }
                    ?>
                ]
            }
        });
        map22.addLayer({
            'id': 'isi',
            'type': 'fill',
            'source': 'maine',
            'layout': {},
            'paint': {
                'fill-color': [
                    "case",
                    ['<', ['get', "dokter_umum_bagi"], 2400], "#008000",
                    ['>', ['get', "dokter_umum_bagi"], 2600], "#FF0000",
                    ['>=', ['get', "dokter_umum_bagi"], 2400], "#FFFF00",
                    ['<=', ['get', "dokter_umum_bagi"], 3600], "#FFFF00",
                    '#123456'
                ],
                'fill-opacity': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    1,
                    0.5
                ]
            }
            // 'paint': {
            //     'fill-color':
            //         '#ffffff',
            //     'fill-opacity': [
            //         'case',
            //         ['boolean', ['feature-state', 'hover'], false],
            //         0,
            //         0
            //     ]
            // }
        });
        map22.addLayer({
            'id': 'batas',
            'type': 'line',
            'source': 'maine',
            'layout': {},
            'paint': {
                'line-width': 2,
                'line-color': '#000000'
            }
        });

        map22.addSource('places', {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': [
                    <?php
                    foreach ($puskesmas as $pk) {
                        $du = Umum::where('puskesmas_id', $pk->id)->where('tahun', $t)->first();
                        $gi = Gigi::where('puskesmas_id', $pk->id)->where('tahun', $t)->first();
                        $pe = Perawat::where('puskesmas_id', $pk->id)->where('tahun', $t)->first();
                        $bi = Bidan::where('puskesmas_id', $pk->id)->where('tahun', $t)->first();
                        echo "{
                                    'type': 'Feature',
                                    'properties': {
                                        'nama': '$pk->nama',
                                        'alamat': '$pk->alamat',
                                        'jenis': '$pk->jenis',
                                        'dokter_umum': ";
                        if (isset($du->jumlah)) {
                            echo "'" . $du->jumlah . "'";
                        } else {
                            echo '0';
                        }
                        echo ",
                                        'dokter_gigi': ";
                        if (isset($gi->jumlah)) {
                            echo "'" . $gi->jumlah . "'";
                        } else {
                            echo '0';
                        }
                        echo ",
                                        'perawat': ";
                        if (isset($pe->jumlah)) {
                            echo "'" . $pe->jumlah . "'";
                        } else {
                            echo '0';
                        }
                        echo ",
                                        'bidan': ";
                        if (isset($bi->jumlah)) {
                            echo "'" . $bi->jumlah . "'";
                        } else {
                            echo '0';
                        }
                        echo ",
                                        'icon': 'theatre'
                                    },
                                    'geometry': {
                                        'type': 'Point',
                                        'coordinates': [
                                            ";
                        echo $pk->lng . ',' . $pk->lat;
                        echo "
                                        ]
                                    }
                                }";
                        echo ',';
                    }
                    ?>
                ]
            }
        });
        // Add a layer showing the places.
        map22.addLayer({
            'id': 'places',
            'type': 'symbol',
            'source': 'places',
            'layout': {
                'icon-image': [
                    "case",
                    [">=",
                        ["to-string", ["get", "jenis"]],
                        ["to-string", "Rawat Inap"]
                    ], "blue", "pink"
                ],
                'icon-allow-overlap': true
            }
        });

        var layerList = document.getElementById('anu');
        var inputs = layerList.getElementsByTagName('input');
        let abc = "umum";

        function switchLayer(layer) {
            var layerId = layer.target.value;
            abc = layerId;
            if (abc == 'dokter umum') {
                map22.setPaintProperty('isi', 'fill-color', ['case', ['<', ['get', "dokter_umum_bagi"], 2400], "#008000",
                    ['>', ['get', "dokter_umum_bagi"], 2600], "#FF0000",
                    ['>=', ['get', "dokter_umum_bagi"], 2400], "#FFFF00",
                    ['<=', ['get', "dokter_umum_bagi"], 3600], "#FFFF00", '#FFFF00'
                ]);
            } else if (abc == 'dokter gigi') {
                map22.setPaintProperty('isi', 'fill-color', ['case', ['<', ['get', "dokter_gigi_bagi"], 1900], "#008000",
                    ['>', ['get', "dokter_gigi_bagi"], 2100], "#FF0000",
                    ['>=', ['get', "dokter_gigi_bagi"], 1900], "#FFFF00",
                    ['<=', ['get', "dokter_gigi_bagi"], 2100], "#FFFF00", '#FFFF00'
                ]);
            } else if (abc == 'perawat') {
                map22.setPaintProperty('isi', 'fill-color', ['case', ['<', ['get', "perawat_bagi"], 755], "#008000",
                    ['>', ['get', "perawat_bagi"], 955], "#FF0000",
                    ['>=', ['get', "perawat_bagi"], 755], "#FFFF00",
                    ['<=', ['get', "perawat_bagi"], 955], "#FFFF00", '#FFFF00'
                ]);
            } else if (abc == 'bidan') {
                map22.setPaintProperty('isi', 'fill-color', ['case', ['<', ['get', "bidan_bagi"], 900], "#008000",
                    ['>', ['get', "bidan_bagi"], 1100], "#FF0000",
                    ['>=', ['get', "bidan_bagi"], 900], "#FFFF00",
                    ['<=', ['get', "bidan_bagi"], 1100], "#FFFF00", '#FFFF00'
                ]);
            }


        }

        for (var i = 0; i < inputs.length; i++) {
            inputs[i].onclick = switchLayer;
        }

        var popup22 = new mapboxgl.Popup({
            closeButton: false,
            closeOnClick: false
        });

        map22.on('mousemove', 'isi', function(e) {
            if (e.features.length > 0) {
                if (hoveredStateId22) {

                    // Change the cursor style as a UI indicator.
                    map22.getCanvas().style.cursor = 'pointer';

                    // Single out the first found feature.
                    var feature22 = e.features[0];
                    let anu = feature22.properties.dokter_umum;
                    let ea = " dokter umum";
                    let karina = feature22.properties.penduduk / anu;
                    if (abc == 'dokter_umum') {
                        ea = " dokter umum";
                        anu = feature22.properties.dokter_umum;
                        karina = feature22.properties.penduduk / anu;
                    } else if (abc == 'dokter_gigi') {
                        ea = " dokter gigi";
                        anu = feature22.properties.dokter_gigi;
                        karina = feature22.properties.penduduk / anu;
                    } else if (abc == 'perawat') {
                        ea = " perawat";
                        anu = feature22.properties.perawat;
                        karina = feature22.properties.penduduk / anu;
                    } else if (abc == 'bidan') {
                        ea = " bidan";
                        anu = feature22.properties.bidan;
                        karina = feature22.properties.penduduk / anu;
                    }


                    // Display a popup with the name of the county
                    popup22.setLngLat(e.lngLat)
                        .setHTML('<h6>' + "Kecamatan " + feature22.properties.nama + '</h6>' + "Total memiliki " + anu + ea + '<br>' + 'Jumlah penduduk ' + feature22.properties.penduduk + '<br>' + "Rata-rata satu" + ea + " melayani " + Math.round(karina) + " penduduk")
                        .addTo(map22);
                    map22.setFeatureState({
                        source: 'maine',
                        id: hoveredStateId22
                    }, {
                        hover: false
                    });
                }
                hoveredStateId22 = e.features[0].id;
                map22.setFeatureState({
                    source: 'maine',
                    id: hoveredStateId22
                }, {
                    hover: true
                });
            }
        });



        map22.on('mouseleave', 'isi', function() {
            if (hoveredStateId22) {
                map22.getCanvas().style.cursor = '';
                popup22.remove();
                map22.setFeatureState({
                    source: 'maine',
                    id: hoveredStateId22
                }, {
                    hover: false
                });
            }
            hoveredStateId22 = null;
        });

        map22.on('idle', function() {
            map22.resize()
        });

        // When a click event occurs on a feature in the places layer, open a popup at the
        // location of the feature, with description HTML from its properties.
        map22.on('click', 'places', function(e) {
            var coordinates22 = e.features[0].geometry.coordinates.slice();
            var nama22 = e.features[0].properties.nama;
            var alamat22 = e.features[0].properties.alamat;
            var jenis22 = e.features[0].properties.jenis;
            var dokter_umum22 = e.features[0].properties.dokter_umum;
            var dokter_gigi22 = e.features[0].properties.dokter_gigi;
            var perawat22 = e.features[0].properties.perawat;
            var bidan22 = e.features[0].properties.bidan;

            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(e.lngLat.lng - coordinates22[0]) > 180) {
                coordinates22[0] += e.lngLat.lng > coordinates22[0] ? 360 : -360;
                2
            }

            let anu2 = e.features[0].properties.dokter_umum;
            let ea2 = " dokter umum";
            if (abc == 'dokter_umum') {
                ea2 = " dokter umum";
                anu2 = e.features[0].properties.dokter_umum;
            } else if (abc == 'dokter_gigi') {
                ea2 = " dokter gigi";
                anu2 = e.features[0].properties.dokter_gigi;
            } else if (abc == 'perawat') {
                ea2 = " perawat";
                anu2 = e.features[0].properties.perawat;
            } else if (abc == 'bidan') {
                ea2 = " bidan";
                anu2 = e.features[0].properties.bidan;
            }

            new mapboxgl.Popup()
                .setLngLat(coordinates22)
                .setHTML('<h6>' + "Puskesmas " + nama22 + '</h6>' + alamat22 + '<br>' + jenis22 + '<br>' + "Memiliki " + anu2 + ea2)
                .addTo(map22);
        });

        // Change the cursor to a pointer when the mouse is over the places layer.
        map22.on('mouseenter', 'places', function() {
            map22.getCanvas().style.cursor = 'pointer';
        });

        // Change it back to a pointer when it leaves.
        map22.on('mouseleave', 'places', function() {
            map22.getCanvas().style.cursor = '';
        });

    });
</script>

<script>
    mapboxgl.accessToken = 'pk.eyJ1IjoiZmlrcmluYW5kYSIsImEiOiJja2RsbmY3djgxMGlxMnlwZDBha213b3hpIn0.wR7buPys4UOuFLbJJqHzIA';
    // Set bounds to New York, New York

    var bounds3 = [
        [110.71253269370584, -8.41837006625697], // Southwest coordinates
        [113.55276180797574, -7.320054630840417] // Northeast coordinates
    ];

    var map3 = new mapboxgl.Map({
        container: 'map3',
        style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        center: [112.11224465310153, -7.841377756255071], // starting position [lng, lat]
        zoom: 8.3,
        maxBounds: device,
    });
    var hoveredStateId3 = null;

    map3.addControl(new mapboxgl.NavigationControl());
    map3.addControl(new mapboxgl.FullscreenControl());

    // map3.on('style.load', function() {
    //     map3.on('click', function(e) {
    //         var coordinates = e.lngLat;
    //         new mapboxgl.Popup()
    //             .setLngLat(coordinates)
    //             .setHTML('you clicked here: <br/>' + coordinates)
    //             .addTo(map3);
    //     });
    // });

    map3.on('load', function() {
        map3.loadImage(
            <?php echo  "'" . asset('icon/mapbox-marker-icon-20px-blue.png') . "'"; ?>,
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map3.addImage('blue', image);
            }
        );

        map3.loadImage(
            <?php echo "'" . asset('icon/mapbox-marker-icon-20px-pink.png') . "'"; ?>,
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map3.addImage('pink', image);
            }
        );

        map3.addSource('maine', {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': [
                    <?php

                    use App\Models\{Kunjungan, Poli};

                    $a = '{';
                    $b = ',';
                    $rawat_jalan = 0;
                    $rawat_inap = 0;

                    foreach ($wilayah as $wly) {
                        $id = substr($wly->id, -4);
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        foreach ($pk as $key2) {
                            $k = Kunjungan::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($k->rawat_jalan)) {
                                $rawat_jalan += $k->rawat_jalan;
                            }
                            if (isset($k->rawat_inap)) {
                                $rawat_inap += $k->rawat_inap;
                            }
                        }
                        echo $a;
                        echo file_get_contents('storage/' . $wly->file); // get the contents, and echo it out.
                        echo $b;
                        echo '"id":' . '"' . $id . '",';
                        echo '"properties": {"nama": ' . '"' . $wly->nama . '"' . ',"wilayah": "Kota Kediri","rawat_jalan":' . '"' . $rawat_jalan . '"' . ',"rawat_inap":' . '"' . $rawat_inap . '"' . '}},';
                        $rawat_jalan = 0;
                        $rawat_inap = 0;
                    }
                    ?>
                ]
            }
        });
        map3.addLayer({
            'id': 'isi',
            'type': 'fill',
            'source': 'maine',
            'layout': {},
            'paint': {
                'fill-color':
                    // "case",
                    // ['<=', ['get', "dokter_umum"], 0], "#FF0000",
                    // ['<=', ['get', "dokter_umum"], 1], "#FFFF00",
                    // ['<=', ['get', "dokter_umum"], 2], "#008000",
                    // ['>', ['get', "dokter_umum"], 2], "#008000",
                    '#ffffff',
                'fill-opacity': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    0,
                    0
                ]
            }
        });
        map3.addLayer({
            'id': 'batas',
            'type': 'line',
            'source': 'maine',
            'layout': {},
            'paint': {
                'line-width': 2,
                'line-color': '#000000'
            }
        });

        map3.addSource('places', {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': [
                    <?php
                    foreach ($puskesmas as $pk) {
                        $kun = Kunjungan::where('puskesmas_id', $pk->id)->where('tahun', $t)->first();
                        echo "{
                                    'type': 'Feature',
                                    'properties': {
                                        'nama': '$pk->nama',
                                        'alamat': '$pk->alamat',
                                        'jenis': '$pk->jenis',
                                        'rawat_jalan': ";
                        if (isset($kun->rawat_jalan)) {
                            echo "'" . $kun->rawat_jalan . "'";
                        } else {
                            echo '0';
                        }
                        echo ",
                                        'rawat_inap': ";
                        if (isset($kun->rawat_inap)) {
                            echo "'" . $kun->rawat_inap . "'";
                        } else {
                            echo '0';
                        }
                        echo ",
                                        'icon': 'theatre'
                                    },
                                    'geometry': {
                                        'type': 'Point',
                                        'coordinates': [
                                            ";
                        echo $pk->lng . ',' . $pk->lat;
                        echo "
                                        ]
                                    }
                                }";
                        echo ',';
                    }
                    ?>
                ]
            }
        });
        // Add a layer showing the places.
        map3.addLayer({
            'id': 'places',
            'type': 'symbol',
            'source': 'places',
            'layout': {
                'icon-image': [
                    "case",
                    [">=",
                        ["to-string", ["get", "jenis"]],
                        ["to-string", "Rawat Inap"]
                    ], "blue", "pink"
                ],
                'icon-allow-overlap': true
            }
        });

        var popup3 = new mapboxgl.Popup({
            closeButton: false,
            closeOnClick: false
        });

        map3.on('mousemove', 'isi', function(e) {
            if (e.features.length > 0) {
                if (hoveredStateId3) {
                    // Change the cursor style as a UI indicator.
                    map3.getCanvas().style.cursor = 'pointer';

                    // Single out the first found feature.
                    var feature3 = e.features[0];
                    if (feature3.properties.rawat_jalan > 0 && feature3.properties.rawat_inap > 0) {
                        var ningning = "Total kunjungan rawat jalan sebanyak " + feature3.properties.rawat_jalan + " penduduk" + '<br>' + "Total kunjungan rawat inap sebanyak " + feature3.properties.rawat_inap + " penduduk";
                    } else if (feature3.properties.rawat_inap == 0) {
                        var ningning = "Total kunjungan rawat jalan sebanyak " + feature3.properties.rawat_jalan + " penduduk";
                    } else if (feature3.properties.rawat_jalan == 0) {
                        var ningning = "Total kunjungan rawat inap sebanyak " + feature3.properties.rawat_inap + " penduduk";
                    } else {
                        var ningning = "Total kunjungan rawat jalan sebanyak 0 penduduk" + '<br>' + "Total kunjungan rawat inap sebanyak 0 penduduk";
                    }

                    // Display a popup with the name of the county
                    popup3.setLngLat(e.lngLat)
                        .setHTML('<h6>' + "Kecamatan " + feature3.properties.nama + '</h6>' + ningning)
                        .addTo(map3);
                    map3.setFeatureState({
                        source: 'maine',
                        id: hoveredStateId3
                    }, {
                        hover: false
                    });
                }
                hoveredStateId3 = e.features[0].id;
                map3.setFeatureState({
                    source: 'maine',
                    id: hoveredStateId3
                }, {
                    hover: true
                });
            }
        });

        map3.on('mouseleave', 'isi', function() {
            if (hoveredStateId3) {
                map3.getCanvas().style.cursor = '';
                popup3.remove();
                map3.setFeatureState({
                    source: 'maine',
                    id: hoveredStateId3
                }, {
                    hover: false
                });
            }
            hoveredStateId3 = null;
        });

        map3.on('idle', function() {
            map3.resize()
        });

        // When a click event occurs on a feature in the places layer, open a popup at the
        // location of the feature, with description HTML from its properties.
        map3.on('click', 'places', function(e) {
            var coordinates3 = e.features[0].geometry.coordinates.slice();
            var nama3 = e.features[0].properties.nama;
            var alamat3 = e.features[0].properties.alamat;
            var jenis3 = e.features[0].properties.jenis;
            var rawat_jalan = e.features[0].properties.rawat_jalan;
            var rawat_inap = e.features[0].properties.rawat_inap;

            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(e.lngLat.lng - coordinates3[0]) > 180) {
                coordinates3[0] += e.lngLat.lng > coordinates3[0] ? 360 : -360;
            }

            if (rawat_jalan > 0 && rawat_inap > 0) {
                var ningnong = "Kunjungan rawat jalan sebanyak " + rawat_jalan + " penduduk" + '<br>' + "Kunjungan rawat inap sebanyak " + rawat_inap + " penduduk";
            } else if (rawat_inap == 0) {
                var ningnong = "Kunjungan rawat jalan sebanyak " + rawat_jalan + " penduduk";
            } else if (rawat_jalan == 0) {
                var ningnong = "Kunjungan rawat inap sebanyak " + rawat_inap + " penduduk";
            } else {
                var ningnong = "Kunjungan rawat jalan sebanyak 0 penduduk" + '<br>' + "Kunjungan rawat inap sebanyak 0 penduduk";
            }

            new mapboxgl.Popup()
                .setLngLat(coordinates3)
                .setHTML('<h6>' + "Puskesmas " + nama3 + '</h6>' + alamat3 + '<br>' + jenis3 + '<br>' + ningnong)
                .addTo(map3);
        });

        // Change the cursor to a pointer when the mouse is over the places layer.
        map3.on('mouseenter', 'places', function() {
            map3.getCanvas().style.cursor = 'pointer';
        });

        // Change it back to a pointer when it leaves.
        map3.on('mouseleave', 'places', function() {
            map3.getCanvas().style.cursor = '';
        });

    });
</script>

<script>
    mapboxgl.accessToken = 'pk.eyJ1IjoiZmlrcmluYW5kYSIsImEiOiJja2RsbmY3djgxMGlxMnlwZDBha213b3hpIn0.wR7buPys4UOuFLbJJqHzIA';
    // Set bounds to New York, New York

    var bounds3 = [
        [110.71253269370584, -8.41837006625697], // Southwest coordinates
        [113.55276180797574, -7.320054630840417] // Northeast coordinates
    ];

    var map4 = new mapboxgl.Map({
        container: 'map4',
        style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        center: [112.11224465310153, -7.841377756255071], // starting position [lng, lat]
        zoom: 8.3,
        maxBounds: device,
    });
    var hoveredStateId4 = null;

    map4.addControl(new mapboxgl.NavigationControl());
    map4.addControl(new mapboxgl.FullscreenControl());

    // map4.on('style.load', function() {
    //     map4.on('click', function(e) {
    //         var coordinates = e.lngLat;
    //         new mapboxgl.Popup()
    //             .setLngLat(coordinates)
    //             .setHTML('you clicked here: <br/>' + coordinates)
    //             .addTo(map4);
    //     });
    // });

    map4.on('load', function() {
        map4.loadImage(
            <?php echo  "'" . asset('icon/mapbox-marker-icon-20px-blue.png') . "'"; ?>,
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map4.addImage('blue', image);
            }
        );

        map4.loadImage(
            <?php echo "'" . asset('icon/mapbox-marker-icon-20px-pink.png') . "'"; ?>,
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map4.addImage('pink', image);
            }
        );

        map4.addSource('maine', {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': [
                    <?php

                    $a = '{';
                    $b = ',';
                    $rawat_jalan = 0;
                    $rawat_inap = 0;

                    foreach ($wilayah as $wly) {
                        $id = substr($wly->id, -4);
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        foreach ($pk as $key2) {
                            $k = Kunjungan::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($k->rawat_jalan)) {
                                $rawat_jalan += $k->rawat_jalan;
                            }
                            if (isset($k->rawat_inap)) {
                                $rawat_inap += $k->rawat_inap;
                            }
                        }
                        echo $a;
                        echo file_get_contents('storage/' . $wly->file); // get the contents, and echo it out.
                        echo $b;
                        echo '"id":' . '"' . $id . '",';
                        echo '"properties": {"nama": ' . '"' . $wly->nama . '"' . ',"wilayah": "Kota Kediri","rawat_jalan":' . '"' . $rawat_jalan . '"' . ',"rawat_inap":' . '"' . $rawat_inap . '"' . '}},';
                        $rawat_jalan = 0;
                        $rawat_inap = 0;
                    }
                    ?>
                ]
            }
        });
        map4.addLayer({
            'id': 'isi',
            'type': 'fill',
            'source': 'maine',
            'layout': {},
            'paint': {
                'fill-color':
                    // "case",
                    // ['<=', ['get', "dokter_umum"], 0], "#FF0000",
                    // ['<=', ['get', "dokter_umum"], 1], "#FFFF00",
                    // ['<=', ['get', "dokter_umum"], 2], "#008000",
                    // ['>', ['get', "dokter_umum"], 2], "#008000",
                    '#ffffff',
                'fill-opacity': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    0,
                    0
                ]
            }
        });
        map4.addLayer({
            'id': 'batas',
            'type': 'line',
            'source': 'maine',
            'layout': {},
            'paint': {
                'line-width': 2,
                'line-color': '#000000'
            }
        });

        map4.addSource('places', {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': [
                    <?php
                    if ($nm == null) {
                        foreach ($puskesmas as $pk) {
                            $fix = array();
                            $p = 0;
                            $p = Poli::where('puskesmas_id', $pk->id)->where('tahun', '<=', $t)->count();
                            $pl = Poli::where('puskesmas_id', $pk->id)->where('tahun', '<=', $t)->get();
                            foreach ($pl as $value2) {
                                $fix[$value2->puskesmas_id][] =  $value2->poli;
                            }
                            $jihan = implode(", ", $fix[$pk->id]);
                            echo "{
                                        'type': 'Feature',
                                        'properties': {
                                            'nama': '$pk->nama',
                                            'alamat': '$pk->alamat',
                                            'total': '$p',
                                            'jenis': '$pk->jenis',
                                            'poli': ";
                            if (!is_null($jihan)) {
                                echo "'" . $jihan . "'";
                            } else {
                                echo '0';
                            }
                            echo ",
                                            'icon': 'theatre'
                                        },
                                        'geometry': {
                                            'type': 'Point',
                                            'coordinates': [
                                                ";
                            echo $pk->lng . ',' . $pk->lat;
                            echo "
                                            ]
                                        }
                                    }";
                            echo ',';
                            $p = 0;
                        }
                    } else {
                        foreach ($aduh as $pk) {
                            $fix = array();
                            $nama = $pk->puskesmas->nama;
                            $alamat = $pk->puskesmas->alamat;
                            $jenis = $pk->puskesmas->jenis;
                            echo "{
                                        'type': 'Feature',
                                        'properties': {
                                            'nama': '$nama',
                                            'alamat': '$alamat',
                                            'jenis': '$jenis',
                                            'poli': ";
                            echo "'" . $nm . "'";
                            echo ",
                                            'icon': 'theatre'
                                        },
                                        'geometry': {
                                            'type': 'Point',
                                            'coordinates': [
                                                ";
                            echo $pk->puskesmas->lng . ',' . $pk->puskesmas->lat;
                            echo "
                                            ]
                                        }
                                    }";
                            echo ',';
                        }
                    }

                    ?>
                ]
            }
        });
        // Add a layer showing the places.
        map4.addLayer({
            'id': 'places',
            'type': 'symbol',
            'source': 'places',
            'layout': {
                'icon-image': [
                    "case",
                    [">=",
                        ["to-string", ["get", "jenis"]],
                        ["to-string", "Rawat Inap"]
                    ], "blue", "pink"
                ],
                'icon-allow-overlap': true
            }
        });

        var popup4 = new mapboxgl.Popup({
            closeButton: false,
            closeOnClick: false
        });

        map4.on('mousemove', 'isi', function(e) {
            if (e.features.length > 0) {
                if (hoveredStateId4) {
                    // Change the cursor style as a UI indicator.
                    map4.getCanvas().style.cursor = 'pointer';

                    // Single out the first found feature.
                    var feature4 = e.features[0];

                    // Display a popup with the name of the county
                    popup4.setLngLat(e.lngLat)
                        .setHTML('<h6>' + "Kecamatan " + feature4.properties.nama + '</h6>')
                        .addTo(map4);
                    map4.setFeatureState({
                        source: 'maine',
                        id: hoveredStateId4
                    }, {
                        hover: false
                    });
                }
                hoveredStateId4 = e.features[0].id;
                map4.setFeatureState({
                    source: 'maine',
                    id: hoveredStateId4
                }, {
                    hover: true
                });
            }
        });

        map4.on('mouseleave', 'isi', function() {
            if (hoveredStateId4) {
                map4.getCanvas().style.cursor = '';
                popup4.remove();
                map4.setFeatureState({
                    source: 'maine',
                    id: hoveredStateId4
                }, {
                    hover: false
                });
            }
            hoveredStateId4 = null;
        });

        map4.on('idle', function() {
            map4.resize()
        });

        // When a click event occurs on a feature in the places layer, open a popup at the
        // location of the feature, with description HTML from its properties.
        map4.on('click', 'places', function(e) {
            var coordinates4 = e.features[0].geometry.coordinates.slice();
            var nama4 = e.features[0].properties.nama;
            var alamat4 = e.features[0].properties.alamat;
            var jenis4 = e.features[0].properties.jenis;
            var poli = e.features[0].properties.poli;

            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(e.lngLat.lng - coordinates4[0]) > 180) {
                coordinates4[0] += e.lngLat.lng > coordinates4[0] ? 360 : -360;
            }

            new mapboxgl.Popup()
                .setLngLat(coordinates4)
                .setHTML('<h6>' + "Puskesmas " + nama4 + '</h6>' + alamat4 + '<br>' + jenis4 + '<br>' + "Tersedia poli : " + poli)
                .addTo(map4);
        });

        // Change the cursor to a pointer when the mouse is over the places layer.
        map4.on('mouseenter', 'places', function() {
            map4.getCanvas().style.cursor = 'pointer';
        });

        // Change it back to a pointer when it leaves.
        map4.on('mouseleave', 'places', function() {
            map4.getCanvas().style.cursor = '';
        });

    });
</script>

<script>
    const chart = Highcharts.chart('jumlah_puskesmas', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Jumlah Puskesmas Standar Kemenkes'
        },
        subtitle: {
            text: '<?php echo "Tahun $t"; ?>'
        },
        xAxis: {
            categories: [
                <?php
                foreach ($wilayah as $wly) {
                    echo '"' . $wly->nama . '",';
                }
                ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah Puskesmas'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Puskesmas Rawat Inap',
            data: [<?php
                    foreach ($wilayah as $wly) {
                        echo Puskesmas::where('wilayah_id', $wly->id)->where('jenis', 'Rawat Inap')->where('tahun', '<=', $t)->count() . ',';
                    }
                    ?>]

        }, {
            name: 'Puskesmas Non Rawat Inap',
            data: [<?php
                    foreach ($wilayah as $wly) {
                        echo Puskesmas::where('wilayah_id', $wly->id)->where('jenis', 'Non Rawat Inap')->where('tahun', '<=', $t)->count() . ',';
                    }
                    ?>]

        }]
    });
    // let wide = false;


    // var element2 = document.getElementById('map1');
    // var positionInfo2 = element2.getBoundingClientRect();
    // var height2 = positionInfo2.height;
    // var width2 = positionInfo2.width;
    // console.log(height2 + 'px', width2 + 'px');


    // document.getElementById('hamburger').addEventListener('click', () => {
    //     var element = document.getElementById('map1');
    //     var positionInfo = element.getBoundingClientRect();
    //     var height = positionInfo.height;
    //     var width = positionInfo.width;
    //     document.getElementById('jumlah_puskesmas').style.width = wide ? width2 + 'px' : '1683px';
    //     wide = !wide;
    //     chart.reflow();
    //     console.log(height + 'px', width + 'px');
    // });



    Highcharts.chart('jumlah_puskesmas2', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Jumlah Puskesmas Terhadap Jumlah Penduduk'
        },
        subtitle: {
            text: '<?php echo "Tahun $t"; ?>'
        },
        xAxis: {
            categories: [
                <?php
                foreach ($wilayah as $wly) {
                    echo '"' . $wly->nama . '",';
                }
                ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Jumlah Penduduk',
            data: [<?php
                    foreach ($wilayah as $wly) {
                        $bar = Penduduk::where('wilayah_id', $wly->id)->where('tahun', '<=', $t);
                        if ($bar->exists()) {
                            $lo = $bar->first()->jumlah;
                        } else {
                            $lo = 0;
                        }
                        echo $lo . ',';
                    }
                    ?>]

        }, {
            name: 'Rata-rata',
            data: [<?php
                    foreach ($wilayah as $wly) {
                        $pus = Puskesmas::where('wilayah_id', $wly->id)->where('tahun', '<=', $t)->count();
                        $real = Penduduk::where('wilayah_id', $wly->id)->where('tahun', '<=', $t);
                        if ($real->exists()) {
                            $pen = $real->first()->jumlah;
                            $hasil = $pen / $pus;
                        } else {
                            $hasil = 0;
                        }
                        echo  round($hasil) . ',';
                    }
                    ?>]

        }]
    });

    Highcharts.chart('tenaga_kesehatan', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Jumlah Tenaga Kesehatan Standar Kemenkes'
        },
        subtitle: {
            text: '<?php echo "Tahun $t"; ?>'
        },
        xAxis: {
            categories: [
                <?php
                foreach ($wilayah as $wly) {
                    echo '"' . $wly->nama . '",';
                }
                ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah Tenaga Kesehatan'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Dokter Umum',
            data: [<?php
                    $j1 = 0;
                    foreach ($wilayah as $wly) {
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        foreach ($pk as $key2) {
                            $d1 = Umum::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($d1->jumlah)) {
                                $j1 += $d1->jumlah;
                            }
                        }
                        echo $j1 . ',';
                        $j1 = 0;
                    }
                    ?>]

        }, {
            name: 'Dokter Gigi',
            data: [<?php
                    $j2 = 0;
                    foreach ($wilayah as $wly) {
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        foreach ($pk as $key2) {
                            $g1 = Gigi::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($g1->jumlah)) {
                                $j2 += $g1->jumlah;
                            }
                        }
                        echo $j2 . ',';
                        $j2 = 0;
                    }
                    ?>]

        }, {
            name: 'Perawat',
            data: [<?php
                    $j3 = 0;
                    foreach ($wilayah as $wly) {
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        foreach ($pk as $key2) {
                            $p1 = Perawat::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($p1->jumlah)) {
                                $j3 += $p1->jumlah;
                            }
                        }
                        echo $j3 . ',';
                        $j3 = 0;
                    }
                    ?>]

        }, {
            name: 'Bidan',
            data: [<?php
                    $j4 = 0;
                    foreach ($wilayah as $wly) {
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        foreach ($pk as $key2) {
                            $b1 = Bidan::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($b1->jumlah)) {
                                $j4 += $b1->jumlah;
                            }
                        }
                        echo $j4 . ',';
                        $j4 = 0;
                    }
                    ?>]

        }]
    });

    Highcharts.chart('tenaga_kesehatan2', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Jumlah Tenaga Kesehatan Standar Kemenkes'
        },
        subtitle: {
            text: '<?php echo "Tahun $t"; ?>'
        },
        xAxis: {
            categories: [
                <?php
                foreach ($wilayah as $wly) {
                    echo '"' . $wly->nama . '",';
                }
                ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah Tenaga Kesehatan'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Dokter Umum',
            data: [<?php
                    $j1 = 0;
                    foreach ($wilayah as $wly) {
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        foreach ($pk as $key2) {
                            $d1 = Umum::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($d1->jumlah)) {
                                $j1 += $d1->jumlah;
                            }
                        }
                        echo $j1 . ',';
                        $j1 = 0;
                    }
                    ?>]

        }, {
            name: 'Dokter Gigi',
            data: [<?php
                    $j2 = 0;
                    foreach ($wilayah as $wly) {
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        foreach ($pk as $key2) {
                            $g1 = Gigi::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($g1->jumlah)) {
                                $j2 += $g1->jumlah;
                            }
                        }
                        echo $j2 . ',';
                        $j2 = 0;
                    }
                    ?>]

        }, {
            name: 'Perawat',
            data: [<?php
                    $j3 = 0;
                    foreach ($wilayah as $wly) {
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        foreach ($pk as $key2) {
                            $p1 = Perawat::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($p1->jumlah)) {
                                $j3 += $p1->jumlah;
                            }
                        }
                        echo $j3 . ',';
                        $j3 = 0;
                    }
                    ?>]

        }, {
            name: 'Bidan',
            data: [<?php
                    $j4 = 0;
                    foreach ($wilayah as $wly) {
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        foreach ($pk as $key2) {
                            $b1 = Bidan::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($b1->jumlah)) {
                                $j4 += $b1->jumlah;
                            }
                        }
                        echo $j4 . ',';
                        $j4 = 0;
                    }
                    ?>]

        }]
    });

    Highcharts.chart('kunjungan', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Jumlah Kunjungan'
        },
        subtitle: {
            text: '<?php echo "Tahun $t"; ?>'
        },
        xAxis: {
            categories: [
                <?php
                foreach ($wilayah as $wly) {
                    echo '"' . $wly->nama . '",';
                }
                ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah Kunjungan'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Rawat Jalan',
            data: [<?php
                    $j10 = 0;
                    foreach ($wilayah as $wly) {
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        foreach ($pk as $key2) {
                            $kun1 = Kunjungan::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($kun1->rawat_jalan)) {
                                $j10 += $kun1->rawat_jalan;
                            }
                        }
                        echo $j10 . ',';
                        $j10 = 0;
                    }
                    ?>]

        }, {
            name: 'Rawat Inap',
            data: [<?php
                    $j20 = 0;
                    foreach ($wilayah as $wly) {
                        $pk = Puskesmas::where('wilayah_id', $wly->id)->get();
                        foreach ($pk as $key2) {
                            $kun2 = Kunjungan::where('puskesmas_id', $key2->id)->where('tahun', $t)->first();
                            if (isset($kun2->rawat_inap)) {
                                $j20 += $kun2->rawat_inap;
                            }
                        }
                        echo $j20 . ',';
                        $j20 = 0;
                    }
                    ?>]

        }]
    });
</script>