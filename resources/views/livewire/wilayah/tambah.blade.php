<div class="content-body" wire:ignore.self>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tambah Wilayah</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="lihat" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Nama</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan nama" wire:model="nama">
                                                <div>
                                                    @error('nama')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">File</label>
                                            <div class="col-sm-9">
                                                <div class="custom-file" wire:ignore>
                                                    <input type="file" class="custom-file-input" wire:model="file">
                                                    <label class="custom-file-label">Choose file</label>
                                                </div>
                                                <div>
                                                    @error('file')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div wire:loading wire:target="file">Uploading...</div>
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary">Lihat</button>
                                            @if($tmp)
                                            <button wire:click="tambah" class="btn btn-success ml-3">Tambah</button>
                                            @endif
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($tmp)
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Peta</h4>
                            </div>
                            <div class="card-body">
                                <div id="map" style="height: 320px;" wire:ignore></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <script type="text/javascript">
                mapboxgl.accessToken = 'pk.eyJ1IjoiZmlrcmluYW5kYSIsImEiOiJja2RsbmY3djgxMGlxMnlwZDBha213b3hpIn0.wR7buPys4UOuFLbJJqHzIA';
                // Set bounds to New York, New York

                var map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
                    center: [111.995085, -7.8111879], // starting position [lng, lat]
                    zoom: 9,
                });

                map.on('idle', function() {
                    map.resize()
                });

                map.on('load', function() {
                    map.addSource('maine', {
                        'type': 'geojson',
                        'data': {
                            'type': 'FeatureCollection',
                            'features': [
                                <?php
                                if ($tmp)
                                    echo '{' . file_get_contents('storage/' . $tmp) . '}'; // get the contents, and echo it out.
                                ?>,
                            ]
                        }
                    });
                    map.addLayer({
                        'id': 'maine',
                        'type': 'fill',
                        'source': 'maine',
                        'layout': {},
                        'paint': {
                            'fill-color': '#088',
                            'fill-opacity': 0.8
                        }
                    });

                    // When a click event occurs on a feature in the states layer, open a popup at the
                    // location of the click, with description HTML from its properties.
                    map.on('click', 'maine', function(e) {
                        new mapboxgl.Popup()
                            .setLngLat(e.lngLat)
                            .setHTML(e.features[0].properties.nama)
                            .addTo(map);
                    });

                    // Change the cursor to a pointer when the mouse is over the states layer.
                    map.on('mouseenter', 'maine', function() {
                        map.getCanvas().style.cursor = 'pointer';
                    });

                    // Change it back to a pointer when it leaves.
                    map.on('mouseleave', 'maine', function() {
                        map.getCanvas().style.cursor = '';
                    });
                });
            </script>
            @endif
        </div>

    </div>
</div>