<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Peta Wilayah {{$nm}}</h4>
                    </div>
                    <div class="card-body">
                        <div id="map" style="height: 560px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    mapboxgl.accessToken = 'pk.eyJ1IjoiZmlrcmluYW5kYSIsImEiOiJja2RsbmY3djgxMGlxMnlwZDBha213b3hpIn0.wR7buPys4UOuFLbJJqHzIA';
    // Set bounds to New York, New York

    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        center: [111.995085, -7.8111879], // starting position [lng, lat]
        zoom: 11,
    });
    var hoveredStateId = null;

    map.on('idle', function() {
        map.resize()
    });

    map.on('load', function() {
        map.loadImage(
            'https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map.addImage('custom-marker', image);

                map.addSource('maine', {
                    'type': 'geojson',
                    'data': {
                        'type': 'FeatureCollection',
                        'features': [
                            <?php
                            $c = '{';
                            $b = ',';
                            echo $c;
                            echo file_get_contents('storage/' . $file); // get the contents, and echo it out.
                            echo $b;
                            echo '}';
                            ?>
                        ]
                    }
                });
                map.addLayer({
                    'id': 'isi',
                    'type': 'fill',
                    'source': 'maine',
                    'layout': {},
                    'paint': {
                        'fill-color': '#627BC1',
                        'fill-opacity': [
                            'case',
                            ['boolean', ['feature-state', 'hover'], false],
                            1,
                            0.5
                        ]
                    }
                });
                map.addLayer({
                    'id': 'batas',
                    'type': 'line',
                    'source': 'maine',
                    'layout': {},
                    'paint': {
                        'line-width': 4,
                        'line-color': '#000000'
                    }
                });

                var popup = new mapboxgl.Popup({
                    closeButton: false,
                    closeOnClick: false
                });

                map.on('mousemove', 'isi', function(e) {
                    if (e.features.length > 0) {
                        if (hoveredStateId) {
                            // Change the cursor style as a UI indicator.
                            map.getCanvas().style.cursor = 'pointer';

                            // Single out the first found feature.
                            var feature = e.features[0];

                            // Display a popup with the name of the county
                            popup.setLngLat(e.lngLat)
                                .setText(feature.properties.nama)
                                .addTo(map);
                            map.setFeatureState({
                                source: 'maine',
                                id: hoveredStateId
                            }, {
                                hover: false
                            });
                        }
                        hoveredStateId = e.features[0].id;
                        map.setFeatureState({
                            source: 'maine',
                            id: hoveredStateId
                        }, {
                            hover: true
                        });
                    }
                });

                map.on('mouseleave', 'isi', function() {
                    if (hoveredStateId) {
                        map.getCanvas().style.cursor = '';
                        popup.remove();
                        map.setFeatureState({
                            source: 'maine',
                            id: hoveredStateId
                        }, {
                            hover: false
                        });
                    }
                    hoveredStateId = null;
                });
            }
        );

    });
</script>