<div class="content-body" wire:ignore.self>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Ubah Profil</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="ubah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Username</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan username" wire:model="username">
                                                <div>
                                                    @error('username')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        @if(auth()->user()->level == 'Petugas')
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Foto</label>
                                            <div class="col-sm-9">
                                                <input type="file" style="color: black;" wire:model="foto2">
                                                <div>
                                                    @error('foto2')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div wire:loading wire:target="foto2">Uploading...</div>
                                                @if ($foto2)
                                                <div class="mt-3" style="object-fit: cover;">
                                                    <div>Photo Preview:</div>
                                                    <img src="{{ $foto2->temporaryUrl() }}" height="400" width="300">
                                                </div>
                                                @else
                                                <div class="mt-3" style="object-fit: cover;">
                                                    <img src="/storage/{{$foto}}" height="400" width="300">
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        @endif
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary">Ubah</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>