<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Puskesmas {{$nm}}</h4>
                    </div>
                    <div class="card-body">
                        <div id="map" style="height: 500px;" wire:ignore></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Poli Puskesmas {{$nm}}</h4>
                        <div class="d-sm-flex align-items-center">
                            <a href="/poli/tambah/{{$nm}}" class="btn btn-outline-primary rounded">Tambah</a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if(count($poli) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Nama</th>
                                        <th>Tahun</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($poli as $key => $p)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$poli->firstItem() + $key}}</strong></td>
                                        <td style="width: 30%;">{{$p->poli}}</td>
                                        <td style="width: 30%;">{{$p->tahun}}</td>
                                        <td style="width: 30%;">
                                            <div class="d-flex">
                                                <a href="/poli/ubah/{{$p->hash}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                                <button wire:click="hapus('{{ $p->hash }}')" class="btn btn-danger shadow sharp" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$poli->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<script>
    mapboxgl.accessToken =
        'pk.eyJ1IjoiZmlrcmluYW5kYSIsImEiOiJja2RsbmY3djgxMGlxMnlwZDBha213b3hpIn0.wR7buPys4UOuFLbJJqHzIA';
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [111.995085, -7.8111879], // starting position [lng, lat]
        zoom: 11,
    });

    map.on('idle', function() {
        map.resize()
    });

    map.on('load', function() {
        map.loadImage(
            'https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',
            // Add an image to use as a custom marker
            function(error, image) {
                if (error) throw error;
                map.addImage('custom-marker', image);

                map.addSource('maine', {
                    'type': 'geojson',
                    'data': {
                        'type': 'FeatureCollection',
                        'features': [
                            <?php
                            $c = '{';
                            $b = ',';
                            echo $c;
                            echo file_get_contents('storage/' . $file); // get the contents, and echo it out.
                            echo $b;
                            echo '}';
                            ?>
                        ]
                    }
                });
                map.addLayer({
                    'id': 'isi',
                    'type': 'fill',
                    'source': 'maine',
                    'layout': {},
                    'paint': {
                        'fill-color': '#627BC1',
                        'fill-opacity': [
                            'case',
                            ['boolean', ['feature-state', 'hover'], false],
                            1,
                            0.5
                        ]
                    }
                });
                map.addLayer({
                    'id': 'batas',
                    'type': 'line',
                    'source': 'maine',
                    'layout': {},
                    'paint': {
                        'line-width': 4,
                        'line-color': '#000000'
                    }
                });

                map.addSource('places', {
                    'type': 'geojson',
                    'data': {
                        'type': 'FeatureCollection',
                        'features': [{
                            'type': 'Feature',
                            'properties': {
                                'description': '<strong>Make it Mount Pleasant</strong><p><a href="http://www.mtpleasantdc.com/makeitmtpleasant" target="_blank" title="Opens in a new window">Make it Mount Pleasant</a> is a handmade and vintage market and afternoon of live entertainment and kids activities. 12:00-6:00 p.m.</p>',
                                'icon': 'theatre'
                            },
                            'geometry': {
                                'type': 'Point',
                                'coordinates': [
                                    <?php
                                    echo $lng . ',' . $lat;
                                    ?>
                                ]
                            }
                        }]
                    }
                });
                // Add a layer showing the places.
                map.addLayer({
                    'id': 'places',
                    'type': 'symbol',
                    'source': 'places',
                    'layout': {
                        'icon-image': 'custom-marker',
                        'icon-allow-overlap': true
                    }
                });


            }
        );

        // When a click event occurs on a feature in the places layer, open a popup at the
        // location of the feature, with description HTML from its properties.
        map.on('click', 'places', function(e) {
            var coordinates = e.features[0].geometry.coordinates.slice();
            var description = e.features[0].properties.description;

            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
            }

            new mapboxgl.Popup()
                .setLngLat(coordinates)
                .setHTML(description)
                .addTo(map);
        });

        // Change the cursor to a pointer when the mouse is over the places layer.
        map.on('mouseenter', 'places', function() {
            map.getCanvas().style.cursor = 'pointer';
        });

        // Change it back to a pointer when it leaves.
        map.on('mouseleave', 'places', function() {
            map.getCanvas().style.cursor = '';
        });

    });
</script>