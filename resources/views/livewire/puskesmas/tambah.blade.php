<div class="content-body" wire:ignore.self>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tambah Puskesmas</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="tambah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Nama</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan nama" wire:model="nama">
                                                <div>
                                                    @error('nama')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Alamat</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" rows="4" wire:model="alamat" style="color: black; resize: none;" placeholder="Masukkan alamat"></textarea>
                                                <div>
                                                    @error('alamat')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Wilayah</label>
                                            <div class="col-sm-9">
                                                <div wire:ignore>
                                                    <select wire:model="wilayah" class="form-control default-select" data-live-search="true">
                                                        <option hidden>Pilih wilayah</option>
                                                        @foreach($wly as $w)
                                                        <option value="{{$w->id}}" style="color: black;">{{$w->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('wilayah')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Latitude</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="lat" style="color: black;" placeholder="Masukkan latitude" wire:model="latitude">
                                                <div>
                                                    @error('latitude')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Longitude</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="lng" style="color: black;" placeholder="Masukkan longitude" wire:model="longitude">
                                                <div>
                                                    @error('longitude')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Jenis</label>
                                            <div class="col-sm-9">
                                                <div wire:ignore>
                                                    <select wire:model="jenis" class="form-control default-select">
                                                        <option hidden>Pilih jenis</option>
                                                        <option value="Rawat Inap" style="color: black;">Rawat Inap</option>
                                                        <option value="Non Rawat Inap" style="color: black;">Non Rawat Inap</option>
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('jenis')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        @foreach($poli as $index => $orderTgs)
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Poli {{$index+1}}</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan poli" wire:model="poli.{{$index}}">
                                                <div>
                                                    @error('poli.' . $index)
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            @if(count($this->poli) > 1)
                                            <div class="col-sm-2">
                                                <button type="submit" class="btn btn-danger btn-xs mt-3" wire:click.prevent="remove({{$index}})">-</button>
                                            </div>
                                            @endif
                                        </div>
                                        @endforeach
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-secondary btn-sm my-3" wire:click.prevent="add">+</button>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Tahun</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan tahun" wire:model="tahun">
                                                <div>
                                                    @error('tahun')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary">Tambah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Peta</h4>
                            </div>
                            <div class="card-body">
                                <div id="map2" style="height: 320px;" wire:ignore></div>
                                <pre id="coordinates" class="coordinates"></pre>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <!-- <script>
                document.getElementById('ayo').addEventListener('click', () => {
                    document.getElementById('my').classList.add('d-none');
                });
            </script> -->
            <script>
                mapboxgl.accessToken = 'pk.eyJ1IjoiZmlrcmluYW5kYSIsImEiOiJja2RsbmY3djgxMGlxMnlwZDBha213b3hpIn0.wR7buPys4UOuFLbJJqHzIA';
                var coordinates = document.getElementById('coordinates');
                var map2 = new mapboxgl.Map({
                    container: 'map2',
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: [111.995085, -7.8111879], // starting position [lng, lat]
                    zoom: 9
                });

                var marker = new mapboxgl.Marker({
                        draggable: true
                    })
                    .setLngLat([111.995085, -7.8111879])
                    .addTo(map2);

                var array1 = {};

                function onDragEnd() {
                    var lngLat = marker.getLngLat();
                    coordinates.style.display = 'block';
                    coordinates.innerHTML =
                        'Longitude: ' + lngLat.lng + '<br />Latitude: ' + lngLat.lat;

                    array1.lat = lngLat.lat;
                    array1.lng = lngLat.lng;
                    console.log(array1);
                    document.getElementById("lat").value = lngLat.lat;
                    document.getElementById("lng").value = lngLat.lng;
                    Livewire.emit('getLatitudeForInput', lngLat.lat);
                    Livewire.emit('getLongitudeForInput', lngLat.lng);

                }

                marker.on('dragend', onDragEnd);
            </script>



        </div>

    </div>
</div>