<div class="content-body" wire:ignore.self>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Ubah Puskesmas</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="lihat" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Nama</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan nama" wire:model="nama">
                                                <div>
                                                    @error('nama')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Alamat</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" rows="4" wire:model="alamat" style="color: black; resize: none;" placeholder="Masukkan alamat"></textarea>
                                                <div>
                                                    @error('alamat')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Wilayah</label>
                                            <div class="col-sm-9">
                                                <div wire:ignore>
                                                    <select wire:model="wilayah" class="form-control default-select" data-live-search="true">
                                                        <option hidden>Pilih wilayah</option>
                                                        @foreach($wly as $w)
                                                        <option value="{{$w->id}}" style="color: black;">{{$w->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('wilayah')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Latitude</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="lat" style="color: black;" placeholder="Masukkan latitude" wire:model="latitude">
                                                <div>
                                                    @error('latitude')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Longitude</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="lng" style="color: black;" placeholder="Masukkan longitude" wire:model="longitude">
                                                <div>
                                                    @error('longitude')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Jenis</label>
                                            <div class="col-sm-9">
                                                <div wire:ignore>
                                                    <select wire:model="jenis" class="form-control default-select">
                                                        <option hidden>Pilih jenis</option>
                                                        <option value="Rawat Inap" style="color: black;">Rawat Inap</option>
                                                        <option value="Non Rawat Inap" style="color: black;">Non Rawat Inap</option>
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('jenis')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Tahun</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan tahun" wire:model="tahun" value="123">
                                                <div>
                                                    @error('tahun')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary" id="ayo">Lihat</button>
                                            @if($lat && $lng)
                                            <button wire:click="tambah" class="btn btn-success ml-3">Ubah</button>
                                            @endif
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="row" id="satu" wire:ignore>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Peta</h4>
                            </div>
                            <div class="card-body">
                                <div id="map2" style="height: 320px;" wire:ignore></div>
                                <pre id="coordinates" class="coordinates"></pre>
                            </div>
                        </div>
                    </div>
                </div>

                @if($lat && $lng)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Peta</h4>
                            </div>
                            <div class="card-body">
                                <div id='map' style='height: 320px;' wire:ignore></div>
                            </div>
                        </div>
                    </div>
                </div>


                <script>
                    function myFunction() {
                        mapboxgl.accessToken =
                            'pk.eyJ1IjoiZmlrcmluYW5kYSIsImEiOiJja2RsbmY3djgxMGlxMnlwZDBha213b3hpIn0.wR7buPys4UOuFLbJJqHzIA';
                        var map = new mapboxgl.Map({
                            container: 'map',
                            style: 'mapbox://styles/mapbox/streets-v11',
                            center: [111.995085, -7.8111879], // starting position [lng, lat]
                            zoom: 11,
                        });

                        map.on('idle', function() {
                            map.resize()
                        });

                        map.on('load', function() {
                            map.loadImage(
                                'https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',
                                // Add an image to use as a custom marker
                                function(error, image) {
                                    if (error) throw error;
                                    map.addImage('custom-marker', image);

                                    map.addSource('maine', {
                                        'type': 'geojson',
                                        'data': {
                                            'type': 'FeatureCollection',
                                            'features': [
                                                <?php
                                                $c = '{';
                                                $b = ',';
                                                echo $c;
                                                echo file_get_contents('storage/' . $a->file); // get the contents, and echo it out.
                                                echo $b;
                                                echo '}';
                                                ?>
                                            ]
                                        }
                                    });
                                    map.addLayer({
                                        'id': 'isi',
                                        'type': 'fill',
                                        'source': 'maine',
                                        'layout': {},
                                        'paint': {
                                            'fill-color': '#627BC1',
                                            'fill-opacity': [
                                                'case',
                                                ['boolean', ['feature-state', 'hover'], false],
                                                1,
                                                0.5
                                            ]
                                        }
                                    });
                                    map.addLayer({
                                        'id': 'batas',
                                        'type': 'line',
                                        'source': 'maine',
                                        'layout': {},
                                        'paint': {
                                            'line-width': 4,
                                            'line-color': '#000000'
                                        }
                                    });

                                    map.addSource('places', {
                                        'type': 'geojson',
                                        'data': {
                                            'type': 'FeatureCollection',
                                            'features': [{
                                                'type': 'Feature',
                                                'properties': {
                                                    'description': '<strong>Make it Mount Pleasant</strong><p><a href="http://www.mtpleasantdc.com/makeitmtpleasant" target="_blank" title="Opens in a new window">Make it Mount Pleasant</a> is a handmade and vintage market and afternoon of live entertainment and kids activities. 12:00-6:00 p.m.</p>',
                                                    'icon': 'theatre'
                                                },
                                                'geometry': {
                                                    'type': 'Point',
                                                    'coordinates': [
                                                        <?php
                                                        echo $lng . ',' . $lat;
                                                        ?>
                                                    ]
                                                }
                                            }]
                                        }
                                    });
                                    // Add a layer showing the places.
                                    map.addLayer({
                                        'id': 'places',
                                        'type': 'symbol',
                                        'source': 'places',
                                        'layout': {
                                            'icon-image': 'custom-marker',
                                            'icon-allow-overlap': true
                                        }
                                    });


                                }
                            );

                            // When a click event occurs on a feature in the places layer, open a popup at the
                            // location of the feature, with description HTML from its properties.
                            map.on('click', 'places', function(e) {
                                var coordinates = e.features[0].geometry.coordinates.slice();
                                var description = e.features[0].properties.description;

                                // Ensure that if the map is zoomed out such that multiple
                                // copies of the feature are visible, the popup appears
                                // over the copy being pointed to.
                                while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                                    coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                                }

                                new mapboxgl.Popup()
                                    .setLngLat(coordinates)
                                    .setHTML(description)
                                    .addTo(map);
                            });

                            // Change the cursor to a pointer when the mouse is over the places layer.
                            map.on('mouseenter', 'places', function() {
                                map.getCanvas().style.cursor = 'pointer';
                            });

                            // Change it back to a pointer when it leaves.
                            map.on('mouseleave', 'places', function() {
                                map.getCanvas().style.cursor = '';
                            });

                        });


                    }

                    $(document).ready(function() {
                        document.getElementById('satu').classList.add('d-none');
                        myFunction();
                        $('#ayo').click(function(e) {
                            document.getElementById('satu').classList.add('d-none');
                            myFunction();
                        });
                    });
                </script>
                @endif
            </div>

            <script>
                mapboxgl.accessToken = 'pk.eyJ1IjoiZmlrcmluYW5kYSIsImEiOiJja2RsbmY3djgxMGlxMnlwZDBha213b3hpIn0.wR7buPys4UOuFLbJJqHzIA';
                var coordinates = document.getElementById('coordinates');
                var map2 = new mapboxgl.Map({
                    container: 'map2',
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: [111.995085, -7.8111879], // starting position [lng, lat]
                    zoom: 11
                });

                map2.on('load', function() {
                    map2.loadImage(
                        'https://docs.mapbox.com/mapbox-gl-js/assets/custom_marker.png',
                        // Add an image to use as a custom marker
                        function(error, image) {
                            if (error) throw error;
                            map2.addImage('custom-marker', image);

                            map2.addSource('maine', {
                                'type': 'geojson',
                                'data': {
                                    'type': 'FeatureCollection',
                                    'features': [
                                        <?php
                                        $c = '{';
                                        $b = ',';
                                        echo $c;
                                        echo file_get_contents('storage/' . $a->file); // get the contents, and echo it out.
                                        echo $b;
                                        echo '}';
                                        ?>
                                    ]
                                }
                            });
                            map2.addLayer({
                                'id': 'isi',
                                'type': 'fill',
                                'source': 'maine',
                                'layout': {},
                                'paint': {
                                    'fill-color': '#627BC1',
                                    'fill-opacity': [
                                        'case',
                                        ['boolean', ['feature-state', 'hover'], false],
                                        1,
                                        0.5
                                    ]
                                }
                            });
                            map2.addLayer({
                                'id': 'batas',
                                'type': 'line',
                                'source': 'maine',
                                'layout': {},
                                'paint': {
                                    'line-width': 4,
                                    'line-color': '#000000'
                                }
                            });




                        }
                    );

                    // When a click event occurs on a feature in the places layer, open a popup at the
                    // location of the feature, with description HTML from its properties.
                    map2.on('click', 'places', function(e) {
                        var coordinates = e.features[0].geometry.coordinates.slice();
                        var description = e.features[0].properties.description;

                        // Ensure that if the map is zoomed out such that multiple
                        // copies of the feature are visible, the popup appears
                        // over the copy being pointed to.
                        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                        }

                        new mapboxgl.Popup()
                            .setLngLat(coordinates)
                            .setHTML(description)
                            .addTo(map2);
                    });

                    // Change the cursor to a pointer when the mouse is over the places layer.
                    map2.on('mouseenter', 'places', function() {
                        map2.getCanvas().style.cursor = 'pointer';
                    });

                    // Change it back to a pointer when it leaves.
                    map2.on('mouseleave', 'places', function() {
                        map2.getCanvas().style.cursor = '';
                    });

                });

                var marker = new mapboxgl.Marker({
                        draggable: true
                    })
                    .setLngLat([<?php
                                echo $longitude . ',' . $latitude;
                                ?>])
                    .addTo(map2);

                var array1 = {};

                function onDragEnd() {
                    var lngLat = marker.getLngLat();
                    coordinates.style.display = 'block';
                    coordinates.innerHTML =
                        'Longitude: ' + lngLat.lng + '<br />Latitude: ' + lngLat.lat;

                    array1.lat = lngLat.lat;
                    array1.lng = lngLat.lng;
                    console.log(array1);
                    document.getElementById("lat").value = lngLat.lat;
                    document.getElementById("lng").value = lngLat.lng;
                    Livewire.emit('getLatitudeForInput', lngLat.lat);
                    Livewire.emit('getLongitudeForInput', lngLat.lng);

                }

                marker.on('dragend', onDragEnd);
            </script>



        </div>

    </div>
</div>