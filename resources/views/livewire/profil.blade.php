<div class="content-body">
    <div class="container-fluid">
        <div class="row gutters-sm">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-end">
                            <a href="/profil/ubah" class="btn btn-info ml-auto">Ubah Profil</a>
                        </div>
                        <div class="d-flex flex-column align-items-center text-center mb-5">
                            @if(auth()->user()->level == 'Petugas')
                            <img src="/storage/{{$user->petugas->foto}}" alt="Admin" width="300" height="350">
                            @else
                            <img src="https://images.unsplash.com/photo-1543132220-4bf3de6e10ae?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80" alt="Admin" width="300" height="350">
                            @endif
                        </div>
                        @if(auth()->user()->level == 'Petugas')
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">NIP</h6>
                            </div>
                            <div class="col-sm-9" style="color:black;">
                                {{$user->petugas->nip}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Nama</h6>
                            </div>
                            <div class="col-sm-9" style="color:black;">
                                {{$user->petugas->nama}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Username</h6>
                            </div>
                            <div class="col-sm-9" style="color:black;">
                                {{$user->username}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Puskesmas</h6>
                            </div>
                            <div class="col-sm-9" style="color:black;">
                                {{$user->petugas->puskesmas->nama}}
                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Username</h6>
                            </div>
                            <div class="col-sm-9" style="color:black;">
                                {{$user->username}}
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>