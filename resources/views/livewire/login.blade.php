<section>
    <div class="page-header section-height-100">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
                    <div class="card card-plain">
                        <div class="card-header pb-0 text-left">
                            <h4 class="font-weight-bolder">Login</h4>
                            <p class="mb-0">Masukkan username dan password</p>
                        </div>
                        <div class="card-body">
                            <form wire:submit.prevent="login" autocomplete="off">
                                <div class="mb-3">
                                    <input type="text" wire:model="username" class="form-control form-control-lg" placeholder="Masukkan username">
                                    <div>
                                        @error('username')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <input type="password" wire:model="password" class="form-control form-control-lg" placeholder="Masukkan password">
                                    <div>
                                        @error('password')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-lg bg-gradient-info btn-lg w-100 mt-4 mb-0">Login</button>
                                </div>
                                @if(session()->get('error'))
                                <div class="text-danger mt-3 text-center">
                                    <strong>
                                        {{ session('error') }}
                                    </strong>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column">
                    <div class="position-relative bg-gradient-info h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center">
                        <img src="{{asset('assets2/img/shapes/pattern-lines.svg')}}" alt="pattern-lines" class="position-absolute opacity-4 start-0">
                        <div class="position-relative">
                            <img class="max-width-500 w-100 position-relative z-index-2" src="https://1.bp.blogspot.com/-oIHwpWi5OOQ/WC5wT7VP08I/AAAAAAAB_lo/zxGr7eC2RZcgWuJC7ZgiPIVqZROtbkFrwCEw/s1600/kemenkes%2B2017.png">
                        </div>
                        <h3 class="mt-5 text-white font-weight-bolder">Sistem Informasi Geografis<br>Persebaran Puskesmas di Kediri Raya</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>