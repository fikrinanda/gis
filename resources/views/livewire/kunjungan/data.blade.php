<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Jumlah Kunjungan Puskesmas {{auth()->user()->petugas->puskesmas->nama}}</h4>
                        <div class="d-sm-flex align-items-center">
                            <a href="/kunjungan/tambah" class="btn btn-outline-primary rounded">Tambah</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage" class="form-control default-select">
                                        <option value="5" style="color: black;">5</option>
                                        <option value="10" style="color: black;">10</option>
                                        <option value="15" style="color: black;">15</option>
                                        <option value="20" style="color: black;">20</option>
                                    </select></label></div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($kunjungan) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Jumlah Kunjungan Rawat Jalan</th>
                                        @if(auth()->user()->petugas->puskesmas->jenis == 'Rawat Inap')
                                        <th>Jumlah Kunjungan Rawat Inap</th>
                                        @endif
                                        <th>Tahun</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($kunjungan as $key => $kun)
                                    <tr>
                                        <td style="width: 12%;"><strong>{{$kunjungan->firstItem() + $key}}</strong></td>
                                        <td style="width: 22%;">{{$kun->rawat_jalan}}</td>
                                        @if(auth()->user()->petugas->puskesmas->jenis == 'Rawat Inap')
                                        <td style="width: 22%;">{{$kun->rawat_inap}}</td>
                                        @endif
                                        <td style="width: 22%;">{{$kun->tahun}}</td>
                                        <td style="width: 22%;">
                                            <div class="d-flex">
                                                <a href="/kunjungan/ubah/{{$kun->tahun}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                                <button wire:click="hapus('{{ $kun->tahun }}')" class="btn btn-danger shadow sharp" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$kunjungan->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>