<div class="content-body" wire:ignore.self>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Ubah Jumlah Dokter Umum</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="tambah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Jumlah</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan jumlah" wire:model="jumlah">
                                                <div>
                                                    @error('jumlah')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Tahun</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan tahun" wire:model="tahun">
                                                <div>
                                                    @error('tahun')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary">Ubah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>