<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Persebaran Puskesmas Kediri Raya</title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/logo-2.png')}}">

    <!-- CSS here -->
    <link rel="stylesheet" href="{{asset('assets4/css/style.css')}}">
    @livewireStyles

    <script src='https://api.mapbox.com/mapbox-gl-js/v2.2.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.2.0/mapbox-gl.css' rel='stylesheet' />

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <style>
        .legend {
            background-color: #fff;
            border-radius: 3px;
            bottom: 30px;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
            font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
            padding: 10px;
            position: absolute;
            right: 10px;
            z-index: 1;
            width: 210px;
        }

        .legend h4 {
            margin: 0 0 10px;
        }

        .legend div span {
            border-radius: 50%;
            display: inline-block;
            height: 10px;
            margin-right: 5px;
            width: 10px;
        }

        .legend2 {
            background-color: #fff;
            border-radius: 3px;
            top: 10px;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
            font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
            padding: 10px;
            position: absolute;
            left: 10px;
            z-index: 1;
            width: 250px;
        }

        .legend2 h4 {
            margin: 0 0 10px;
        }

        .legend2 div span {
            border-radius: 50%;
            display: inline-block;
            height: 10px;
            margin-left: 5px;
            width: 10px;
        }

        .marker {
            background-image: url('mapbox-icon.png');
            background-size: cover;
            width: 50px;
            height: 50px;
            border-radius: 50%;
            cursor: pointer;
        }

        .mapboxgl-popup-content {
            width: 250px;
            font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
        }

        .coordinates {
            background: rgba(0, 0, 0, 0.5);
            color: #fff;
            position: absolute;
            bottom: 60px;
            left: 40px;
            padding: 5px 10px;
            margin: 0;
            font-size: 11px;
            line-height: 18px;
            border-radius: 3px;
            display: none;
        }

        #anu {
            position: absolute;
            top: 10px;
            left: 10px;
            background: #efefef;
            padding: 10px;
            font: 14px/24px 'Helvetica Neue', Arial, Helvetica, sans-serif;
            z-index: 1;
        }

        #anu22 {
            position: absolute;
            top: 10px;
            left: 10px;
            background: #efefef;
            padding: 10px;
            font: 14px/24px 'Helvetica Neue', Arial, Helvetica, sans-serif;
            z-index: 1;
        }
    </style>

    @livewireStyles
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- /Preloader -->

    <!-- Header Area Start -->


    <!-- Header Area End -->

    <!-- Welcome-area-start -->
    <div class="welcome-area home-6 theme-bg" id="home" style="height: 100%;">
        <div class="welcome-bg-thumb home-3 opacity-6" style="background-image: url(assets4/img/bg-img/bg-patter.png);">
        </div>
        <div class="container pb-5">

            <div class="welcome-content home-3 text-center mb-5">
                <h3>&nbsp;</h3>
                <h3>Sistem Informasi Geografis</h3>
                <h3>Persebaran Puskesmas di Kediri Raya</h3>
                <h3>Tahun {{$tahun}}</h3>
            </div>
            @yield('content')

        </div>
    </div>
    <!-- Welcome-area-end -->

    @livewireScripts
    <!-- JS here -->
    <script src="{{asset('assets4/js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{asset('assets4/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{asset('assets4/js/popper.min.js')}}"></script>
    <script src="{{asset('assets4/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets4/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets4/js/jquery.nav.js')}}"></script>
    <script src="{{asset('assets4/js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('assets4/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('assets4/js/waypoints.min.js')}}"></script>
    <script src="{{asset('assets4/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets4/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('assets4/js/scrollIt.js')}}"></script>
    <script src="{{asset('assets4/js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{asset('assets4/js/wow.min.js')}}"></script>
    <script src="{{asset('assets4/js/nice-select.min.js')}}"></script>
    <script src="{{asset('assets4/js/jquery.slicknav.min.js')}}"></script>
    <script src="{{asset('assets4/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('assets4/js/slick.min.js')}}"></script>
    <script src="{{asset('assets4/js/jquery.animatedheadline.min.js')}}"></script>
    <script src="{{asset('assets4/js/classy-nav.js')}}"></script>

    <!-- Custom js-->
    <script src="{{asset('assets4/js/main.js')}}"></script>

    @yield('js')
    @livewireScripts
</body>

</html>