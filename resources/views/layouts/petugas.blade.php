<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $title ?? config('app.name') }}</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/logo-2.png')}}">
    <link href="{{asset('assets/vendor/jqvmap/css/jqvmap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/vendor/chartist/css/chartist.min.css')}}">
    <link href="{{asset('assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">

    <script src='https://api.mapbox.com/mapbox-gl-js/v2.3.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.3.0/mapbox-gl.css' rel='stylesheet' />

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <style>
        .legend {
            background-color: #fff;
            border-radius: 3px;
            bottom: 30px;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
            font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
            padding: 10px;
            position: absolute;
            right: 10px;
            z-index: 1;
        }

        .legend h4 {
            margin: 0 0 10px;
        }

        .legend div span {
            border-radius: 50%;
            display: inline-block;
            height: 10px;
            margin-right: 5px;
            width: 10px;
        }

        .marker {
            background-image: url('mapbox-icon.png');
            background-size: cover;
            width: 50px;
            height: 50px;
            border-radius: 50%;
            cursor: pointer;
        }

        .mapboxgl-popup {
            max-width: 400px;
            font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
        }

        #anu {
            position: absolute;
            top: 10px;
            left: 10px;
            background: #efefef;
            padding: 10px;
            font: 14px/24px 'Helvetica Neue', Arial, Helvetica, sans-serif;
            z-index: 1;
        }

        #anu22 {
            position: absolute;
            top: 10px;
            left: 10px;
            background: #efefef;
            padding: 10px;
            font: 14px/24px 'Helvetica Neue', Arial, Helvetica, sans-serif;
            z-index: 1;
        }
    </style>

    @livewireStyles
</head>

<body>
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">
        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="/" class="brand-logo">
                <img class="logo-abbr" src="https://1.bp.blogspot.com/-oIHwpWi5OOQ/WC5wT7VP08I/AAAAAAAB_lo/zxGr7eC2RZcgWuJC7ZgiPIVqZROtbkFrwCEw/s1600/kemenkes%2B2017.png" alt="" style="max-width: 500px; width: 100px;">
                <img class="logo-compact" src="" alt="">
                <img class="brand-title" src="" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <div class="dashboard_bar">

                            </div>
                        </div>
                        <ul class="navbar-nav header-right">
                            <li class="nav-item dropdown header-profile">
                                <a class="nav-link" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                    <div class="header-info">
                                        <span class="text-black"><strong>{{auth()->user()->petugas->nama}}</strong></span>
                                        <p class="fs-12 mb-0">Petugas</p>
                                    </div>
                                    <img src="/storage/{{auth()->user()->petugas->foto}}" width="20" alt="" />
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="/profil" class="dropdown-item ai-icon">
                                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user" width="18" height="18" class="svg-inline--fa fa-user text-primary" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor" d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"></path>
                                        </svg>
                                        <span class="ml-2">Profil </span>
                                    </a>
                                    <a href="/password/ubah" class="dropdown-item ai-icon">
                                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="key" width="18" height="18" class="svg-inline--fa fa-key text-success" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                            <path fill="currentColor" d="M512 176.001C512 273.203 433.202 352 336 352c-11.22 0-22.19-1.062-32.827-3.069l-24.012 27.014A23.999 23.999 0 0 1 261.223 384H224v40c0 13.255-10.745 24-24 24h-40v40c0 13.255-10.745 24-24 24H24c-13.255 0-24-10.745-24-24v-78.059c0-6.365 2.529-12.47 7.029-16.971l161.802-161.802C163.108 213.814 160 195.271 160 176 160 78.798 238.797.001 335.999 0 433.488-.001 512 78.511 512 176.001zM336 128c0 26.51 21.49 48 48 48s48-21.49 48-48-21.49-48-48-48-48 21.49-48 48z"></path>
                                        </svg>
                                        <span class="ml-2">Ubah Password </span>
                                    </a>
                                    <form action="/logout" method="post" id="form1">
                                        @csrf
                                        <button type="submit" class="dropdown-item ai-icon">
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="sign-out-alt" width="18" height="18" class="svg-inline--fa fa-sign-out-alt text-danger" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor" d="M497 273L329 441c-15 15-41 4.5-41-17v-96H152c-13.3 0-24-10.7-24-24v-96c0-13.3 10.7-24 24-24h136V88c0-21.4 25.9-32 41-17l168 168c9.3 9.4 9.3 24.6 0 34zM192 436v-40c0-6.6-5.4-12-12-12H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h84c6.6 0 12-5.4 12-12V76c0-6.6-5.4-12-12-12H96c-53 0-96 43-96 96v192c0 53 43 96 96 96h84c6.6 0 12-5.4 12-12z"></path>
                                            </svg>
                                            <span class="ml-2">Logout </span>
                                        </button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="deznav">
            <div class="deznav-scroll">
                <ul class="metismenu" id="menu">
                    <li {{ request()->is('petugas/*') ? " class=mm-active" : ''}}><a href="/petugas" class="ai-icon" aria-expanded="false">
                            <i class="flaticon-381-home"></i>
                            <span class="nav-text">Dashboard</span>
                        </a>
                    </li>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                            <i class="flaticon-381-user-9"></i>
                            <span class="nav-text" style="font-size: 16px;">Tenaga Kesehatan</span>
                        </a>
                        <ul aria-expanded="false">
                            <li {{ request()->is('umum/*') ? " class=mm-active" : ''}}><a href="/umum/data" {{ request()->is('umum/*') ? " class=mm-active" : ''}}>Dokter Umum</a></li>
                            <li {{ request()->is('gigi/*') ? " class=mm-active" : ''}}><a href="/gigi/data" {{ request()->is('gigi/*') ? " class=mm-active" : ''}}>Dokter Gigi</a></li>
                            <li {{ request()->is('perawat/*') ? " class=mm-active" : ''}}><a href="/perawat/data" {{ request()->is('perawat/*') ? " class=mm-active" : ''}}>Perawat</a></li>
                            <li {{ request()->is('bidan/*') ? " class=mm-active" : ''}}><a href="/bidan/data" {{ request()->is('bidan/*') ? " class=mm-active" : ''}}>Bidan</a></li>
                        </ul>
                    </li>
                    <li {{ request()->is('kunjungan/*') ? " class=mm-active" : ''}}><a href="/kunjungan/data" class="ai-icon" aria-expanded="false">
                            <i class="flaticon-381-folder-9"></i>
                            <span class="nav-text">Kunjungan</span>
                        </a>
                    </li>

                </ul>

                <!-- <div class="copyright">
                    <p><strong>Sistem Informasi Geografis<br>Persebaran Puskesmas<br>Kediri Raya</strong> © 2021 All Rights Reserved</p>
                </div> -->
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->

        @yield('content')

        <!--**********************************
            Footer start
        ***********************************-->
        <!-- <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed &amp; Developed with <span class="heart"></span> by <a href="http://dexignzone.com/" target="_blank">DexignZone</a> & FikriNanda 2021</p>
            </div>
        </div> -->
        <!--**********************************
            Footer end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="{{asset('assets/vendor/global/global.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('assets/vendor/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/custom.min.js')}}"></script>
    <script src="{{asset('assets/js/deznav-init.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        const SwalModal = (icon, title, html) => {
            Swal.fire({
                icon,
                title,
                html
            }).then(result => {
                return livewire.emit('berhasil');
            })
        }

        const SwalConfirm = (icon, title, html, confirmButtonText, method, params, callback) => {
            Swal.fire({
                icon,
                title,
                html,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText,
                reverseButtons: true,
            }).then(result => {
                if (result.value) {
                    return livewire.emit('yakin', params);
                } else {
                    return livewire.emit('batal');
                }

                if (callback) {
                    return livewire.emit('batal');
                }
            })
        }

        const SwalAlert = (icon, title, timeout = 1000) => {
            const Toast = Swal.mixin({
                toast: true,
                position: 'center',
                showConfirmButton: false,
                timer: timeout,
                onOpen: toast => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon,
                title
            })
        }

        document.addEventListener('DOMContentLoaded', () => {
            this.livewire.on('swal:modal', data => {
                SwalModal(data.icon, data.title, data.text)
            })

            this.livewire.on('swal:confirm', data => {
                SwalConfirm(data.icon, data.title, data.text, data.confirmText, data.method, data.params, data.callback)
            })

            this.livewire.on('swal:alert', data => {
                SwalAlert(data.icon, data.title, data.timeout)
            })
        })
    </script>

    @yield('js')
    @livewireScripts
</body>

</html>