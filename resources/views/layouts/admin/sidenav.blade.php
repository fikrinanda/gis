<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="../../pages/dashboards/dashboard.html">
                <img src="../../assets/img/brand/blue.png" class="navbar-brand-img" alt="...">
            </a>
            <div class=" ml-auto ">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link{{request()->is('admin') ? ' active' : ''}}" href="/admin">
                            <i class="fas fa-home{{request()->is('admin') ? ' text-default' : ' text-info'}}" style="font-size: 15px;"></i>
                            <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="petugas.html">
                            <i class="fas fa-user text-info" style="font-size: 15px;"></i>
                            <span class="nav-link-text">Data Petugas</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{request()->is('wilayah') ? ' active' : ''}}{{request()->is('wilayah/*') ? ' active' : ''}}" href="/wilayah">
                            <i class="fas fa-map text-info{{request()->is('wilayah') ? ' text-default' : ' text-info'}}{{request()->is('wilayah/*') ? ' text-default' : ' text-info'}}" style="font-size: 15px;"></i>
                            <span class="nav-link-text">Data Wilayah</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{request()->is('puskesmas') ? ' active' : ''}}{{request()->is('puskesmas/*') ? ' active' : ''}}" href="/puskesmas">
                            <i class="fas fa-hospital text-info{{request()->is('puskesmas') ? ' text-default' : ' text-info'}}{{request()->is('puskesmas/*') ? ' text-default' : ' text-info'}}" style="font-size: 15px;"></i>
                            <span class="nav-link-text">Data Puskesmas</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>