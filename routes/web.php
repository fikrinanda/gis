<?php

use App\Http\Controllers\Controller;
use App\Http\Livewire\Admin\Index as AdminIndex;
use App\Http\Livewire\Petugas\Index as PetugasIndex;
use App\Http\Livewire\Login;
use App\Http\Livewire\Profil;
use App\Http\Livewire\Ubah;
use App\Http\Livewire\Password;
use App\Http\Livewire\Logout;
use App\Http\Livewire\Anu;
use App\Http\Livewire\Puskesmas\Index as PuskesmasIndex;
use App\Http\Livewire\Puskesmas\Tambah as PuskesmasTambah;
use App\Http\Livewire\Puskesmas\Lihat as PuskesmasLihat;
use App\Http\Livewire\Puskesmas\Ubah as PuskesmasUbah;
use App\Http\Livewire\Wilayah\Index as WilayahIndex;
use App\Http\Livewire\Wilayah\Tambah as WilayahTambah;
use App\Http\Livewire\Wilayah\Lihat as WilayahLihat;
use App\Http\Livewire\Wilayah\Ubah as WilayahUbah;
use App\Http\Livewire\Petugas\Data as PetugasData;
use App\Http\Livewire\Petugas\Tambah as PetugasTambah;
use App\Http\Livewire\Petugas\Ubah as PetugasUbah;
use App\Http\Livewire\Petugas\Lihat as PetugasLihat;
use App\Http\Livewire\Umum\Data as UmumData;
use App\Http\Livewire\Umum\Tambah as UmumTambah;
use App\Http\Livewire\Umum\Ubah as UmumUbah;
use App\Http\Livewire\Gigi\Data as GigiData;
use App\Http\Livewire\Gigi\Tambah as GigiTambah;
use App\Http\Livewire\Gigi\Ubah as GigiUbah;
use App\Http\Livewire\Perawat\Data as PerawatData;
use App\Http\Livewire\Perawat\Tambah as PerawatTambah;
use App\Http\Livewire\Perawat\Ubah as PerawatUbah;
use App\Http\Livewire\Bidan\Data as BidanData;
use App\Http\Livewire\Bidan\Tambah as BidanTambah;
use App\Http\Livewire\Bidan\Ubah as BidanUbah;
use App\Http\Livewire\Kunjungan\Data as KunjunganData;
use App\Http\Livewire\Kunjungan\Tambah as KunjunganTambah;
use App\Http\Livewire\Kunjungan\Ubah as KunjunganUbah;
use App\Http\Livewire\Poli\Tambah as PoliTambah;
use App\Http\Livewire\Poli\Ubah as PoliUbah;
use App\Http\Livewire\Penduduk\Data as PendudukData;
use App\Http\Livewire\Penduduk\Tambah as PendudukTambah;
use App\Http\Livewire\Penduduk\Ubah as PendudukUbah;
use App\Models\Penduduk;
use App\Models\Poli;
use App\Models\Puskesmas;
use App\Models\Umum;
use App\Models\Wilayah;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/coba', function () {
    $umum = Umum::where('tahun', '2021')->sum('jumlah');
    dd($umum);
});

Route::middleware('guest')->group(function () {
    Route::get('/login', Login::class)->name('login');
});

Route::get('/import', [Controller::class, 'show']);
Route::post('/import/wilayah', [Controller::class, 'wilayah']);
Route::post('/import/puskesmas', [Controller::class, 'puskesmas']);
Route::post('/import/umum', [Controller::class, 'umum']);
Route::post('/import/penduduk', [Controller::class, 'penduduk']);

Route::post('/logout', Controller::class)->middleware('auth');
Route::get('/profil', Profil::class)->middleware('auth');
Route::get('/profil/ubah', Ubah::class)->middleware('auth');
Route::get('/password/ubah', Password::class)->middleware('auth');
Route::get('/log', Logout::class)->middleware('auth');

Route::middleware('auth', 'cekAdmin')->group(function () {
    Route::get('/admin', AdminIndex::class);
    Route::get('/admin/{thn}', AdminIndex::class);
    Route::get('/admin/{thn}/{nama}', AdminIndex::class);
    Route::get('/wilayah/data', WilayahIndex::class);
    Route::get('/wilayah/tambah', WilayahTambah::class);
    Route::get('/wilayah/lihat/{nama}', WilayahLihat::class);
    Route::get('/wilayah/ubah/{nama}', WilayahUbah::class);
    Route::get('/puskesmas/data', PuskesmasIndex::class);
    Route::get('/puskesmas/tambah/', PuskesmasTambah::class);
    Route::get('/puskesmas/lihat/{nama}', PuskesmasLihat::class);
    Route::get('/puskesmas/ubah/{nama}', PuskesmasUbah::class);
    Route::get('/petugas/data', PetugasData::class);
    Route::get('/petugas/tambah', PetugasTambah::class);
    Route::get('/petugas/ubah/{username}', PetugasUbah::class);
    Route::get('/petugas/lihat/{username}', PetugasLihat::class);
    Route::get('/poli/tambah/{nama}', PoliTambah::class);
    Route::get('/poli/ubah/{hash}', PoliUbah::class);
    Route::get('/penduduk/data', PendudukData::class);
    Route::get('/penduduk/tambah', PendudukTambah::class);
    Route::get('/penduduk/ubah/{wilayah_id}/{tahun}', PendudukUbah::class);
});

Route::middleware('auth', 'cekPetugas')->group(function () {
    Route::get('/petugas', PetugasIndex::class);
    Route::get('/petugas/{thn}', PetugasIndex::class);
    Route::get('/petugas/{thn}/{nama}', PetugasIndex::class);
    Route::get('/umum/data', UmumData::class);
    Route::get('/umum/tambah', UmumTambah::class);
    Route::get('/umum/ubah/{tahun}', UmumUbah::class);
    Route::get('/gigi/data', GigiData::class);
    Route::get('/gigi/tambah', GigiTambah::class);
    Route::get('/gigi/ubah/{tahun}', GigiUbah::class);
    Route::get('/perawat/data', PerawatData::class);
    Route::get('/perawat/tambah', PerawatTambah::class);
    Route::get('/perawat/ubah/{tahun}', PerawatUbah::class);
    Route::get('/bidan/data', BidanData::class);
    Route::get('/bidan/tambah', BidanTambah::class);
    Route::get('/bidan/ubah/{tahun}', BidanUbah::class);
    Route::get('/kunjungan/data', KunjunganData::class);
    Route::get('/kunjungan/tambah', KunjunganTambah::class);
    Route::get('/kunjungan/ubah/{tahun}', KunjunganUbah::class);
});

Route::get('/', Anu::class);
Route::get('/{thn}', Anu::class);
Route::get('/{thn}/{nama}', Anu::class);
